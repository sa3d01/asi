<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{url('admin')}}" class="waves-effect"><i class="ti-home"></i> <span> لوحة التحكم </span></a>
                </li>

                <li class="text-muted menu-title"></li>
                <li class="has_sub">
                    <a href="{{route('setting.get_setting')}}" class="waves-effect"><i class="md md-settings"></i><span> الإعدادات </span></a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-location-city"></i><span class="label label-danger pull-right">{{$con_count}}</span><span> الرسائل </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('contact.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="ti-text"></i><span class="label label-warning pull-right">{{$lang_count}}</span><span> لغات الموقع </span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="{{route('language.create')}}">إضافة لغة جديد</a></li>--}}
                        {{--<li><a href="{{route('language.index')}}">عرض الكل</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-panel"></i><span class="label label-warning pull-right">{{$achievement_count}}</span><span> الانجازات </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('achievement.create')}}">إضافة انجاز جديد</a></li>
                        <li><a href="{{route('achievement.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-panel"></i><span class="label label-warning pull-right">{{$branche_count}}</span><span> الفروع </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('branche.create')}}">إضافة فرع جديد</a></li>
                        <li><a href="{{route('branche.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="ti-panel"></i><span class="label label-warning pull-right">{{$page_count}}</span><span> الصفحات التعريفية </span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="{{route('page.create')}}">إضافة صفحة جديد</a></li>--}}
                        {{--<li><a href="{{route('page.index')}}">عرض الكل</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="text-muted menu-title"></li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i><span class="label label-info pull-right">{{$admin_count}}</span><span> الإدارة </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('admin.create')}}">إضافة مدير جديد</a></li>
                        <li><a href="{{route('admin.index')}}">عرض الكل</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-store-mall-directory"></i><span class="label label-info pull-right">{{$article_count}}</span><span> الأخبار </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('article.create')}}">إضافة خبر جديد</a></li>
                        <li><a href="{{route('article.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-location-city"></i><span> خبراتنا </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('experience.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span class="label label-info pull-right">{{$user_count}}</span><span> الأعضاء </span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="{{route('user.create')}}">إضافة عضو جديد</a></li>--}}
                        {{--<li><a href="{{route('user.index')}}">عرض الكل</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-location-city"></i><span class="label label-info pull-right">{{$feedback_count}}</span><span> اراء العمﻻء </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('feedback.create')}}">إضافة رأى جديد</a></li>
                        <li><a href="{{route('feedback.index')}}">عرض الكل</a></li>
                    </ul>
                </li>

                <li class="text-muted menu-title"></li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-landscape"></i><span> الاستضافات </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('host.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-format-line-spacing"></i><span> الوظائف المتاحة </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('jop.create')}}">إضافة وظيفة جديد</a></li>
                        <li><a href="{{route('jop.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-format-line-spacing"></i><span> شركائنا </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('partener.create')}}">إضافة شريك جديد</a></li>
                        <li><a href="{{route('partener.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-format-line-spacing"></i><span> منتجاتنا </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('product.create')}}">إضافة منتج جديد</a></li>
                        <li><a href="{{route('product.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-format-line-spacing"></i><span> فريق العمل </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('team.create')}}">إضافة عضو جديد</a></li>
                        <li><a href="{{route('team.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md md-perm-media"></i><span> وسائل التواصل  </span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('social.create')}}">إضافة رابط جديد</a></li>
                        <li><a href="{{route('social.index')}}">عرض الكل</a></li>
                    </ul>
                </li>
                <li class="text-muted menu-title"></li>

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->