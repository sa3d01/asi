@extends('admin.layouts.app')
@section('title',$module_name)
@section('style')
    <link href="{{asset('panel/assets/plugins/summernote/summernote.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                <h4 class="m-t-0 header-title"><b><i class="icon-pencil before_word"></i>&nbsp;
                        تعديل بيانات {{ $single_module_name }}
                    </b>
                    <hr>
                </h4>
                {!! Form::model($row, ['method'=>'patch','name'=>'update', 'files'=>true, 'route'=>[$route.'.update', $row->id], 'class' => 'form-horizontal form-row-seperated']) !!}
                    {!! Form::hidden('id', $row->id) !!}
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($languages as $language)
                                <div>
                                    <label for="title">العنوان بـ{{$language->name}}</label>
                                    @php $title=\App\Models\ProductDescription::where(['language_id'=>$language->id,'product_id'=>$row->id])->value('title'); @endphp
                                    {!! Form::text('title_'.$language->label, $title, ['class'=>'form-control','required']) !!}
                                    @if ($errors->has('title_'.$language->label))
                                        <small class="text-danger">{{ $errors->first('name_'.$language->label) }}</small>
                                    @endif
                                </div>
                            @endforeach
                            @foreach($languages as $language)
                                <div>
                                    <label for="title">تفاصيل بـ{{$language->name}}</label>
                                    @php $note=\App\Models\ProductDescription::where(['language_id'=>$language->id,'product_id'=>$row->id])->value('note'); @endphp
                                    {!! Form::textarea('note_'.$language->label, $note , ['class'=>'form-control']) !!}
                                    @if ($errors->has('note_'.$language->label))
                                        <small class="text-danger">{{ $errors->first('note_'.$language->label) }}</small>
                                    @endif
                                </div>
                            @endforeach
                                <br>
                                <div class="form-group">
                                    <div class="fileupload btn btn-purple waves-effect waves-light">
                                        <span><i class="ion-upload m-r-12"></i>صور المنتج</span>
                                        <input class="upload" type="file" accept="image/*" name="images[]" multiple />
                                        @if ($errors->has('images'))
                                            <small class="text-danger">{{ $errors->first('images') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="fileupload btn btn-purple waves-effect waves-light">
                                        <span><i class="ion-upload m-r-12"></i>فديو المنتج</span>
                                        <input class="upload" type="file" accept="video/*" name="video" />
                                        @if ($errors->has('video'))
                                            <small class="text-danger">{{ $errors->first('video') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <div>
                                    <label for="title">رابط المنتج android</label>
                                    {!! Form::text('android', $row->android, ['class'=>'form-control']) !!}
                                    @if ($errors->has('android'))
                                        <small class="text-danger">{{ $errors->first('android') }}</small>
                                    @endif
                                </div>
                                <div>
                                    <label for="title">رابط المنتج ios</label>
                                    {!! Form::text('ios', $row->ios, ['class'=>'form-control']) !!}
                                    @if ($errors->has('ios'))
                                        <small class="text-danger">{{ $errors->first('ios') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">ينتمى الى</label>
                                    {{ Form::select('type', $types, $row->type, array('class' => 'form-control select2 select2-hidden-accessible','tabindex'=>-1,'aria-hidden'=>true)) }}
                                    @if ($errors->has('type'))
                                        <small class="text-danger">{{ $errors->first('type') }}</small>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="control-label col-md-push-1">
                            <button type="submit" class="update_button btn btn-success btn-rounded waves-effect waves-light">
                                تعديل
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{asset('panel/assets/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            $('.summernote').summernote({
                height: 350,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });
            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
@stop