@extends('admin.layouts.app')
@section('title',$module_name)
@section('style')
    <link href="{{asset('panel/assets/plugins/summernote/summernote.css')}}" rel="stylesheet" />
    <link href="{{asset('panel/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('panel/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><i class="icon-pencil before_word"></i>&nbsp;
                                إضافة {{ $single_module_name }}
                            </b>
                            <hr>
                        </h4>
                        {!! Form::open(['method'=>'post', 'files'=>true, 'enctype' => 'multipart/form-data', 'route'=>[$route.'.store'], 'class' => 'form-row-seperated add_ads_form']) !!}
                        {!! Form::hidden('add_by', Auth::user()->id) !!}
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($languages as $language)
                                    <div>
                                        <label for="title">اسم المسئول {{$language->name}}</label>
                                        {!! Form::text('manager_'.$language->label, null, ['class'=>'form-control']) !!}
                                        @if ($errors->has('manager_'.$language->label))
                                            <small class="text-danger">{{ $errors->first('manager_'.$language->label)}}</small>
                                        @endif
                                    </div>
                                @endforeach
                                @foreach($languages as $language)
                                    <div>
                                        <label for="title">العنوان بـ{{$language->name}}</label>
                                        {!! Form::text('title_'.$language->label, null, ['class'=>'form-control']) !!}
                                        @if ($errors->has('title_'.$language->label))
                                            <small class="text-danger">{{ $errors->first('title_'.$language->label) }}</small>
                                        @endif
                                    </div>
                                @endforeach
                                @foreach($languages as $language)
                                    <div>
                                        <label for="title">تفاصيل بـ{{$language->name}}</label>
                                        {!! Form::textarea('note_'.$language->label, null , ['class'=>'form-control']) !!}
                                        @if ($errors->has('note_'.$language->label))
                                            <small class="text-danger">{{ $errors->first('note_'.$language->label) }}</small>
                                        @endif
                                    </div>
                                @endforeach
                                    <div class="form-group">
                                        <label class="control-label">تاريخ الأنجاز</label>
                                        <div class="input-group">
                                            <input required type="text" name="date" class="form-control" placeholder="mm/dd/yyyy" id="start_period">
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-md-push-1">
                                <button type="submit" class="update_button btn btn-success btn-rounded waves-effect waves-light">
                                    إضافة
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('panel/assets/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            $('.summernote').summernote({
                height: 350,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });
            $('.inline-editor').summernote({
                airMode: true
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $( "#start_period" ).datepicker();
            $( "#end_period" ).datepicker();
        });
    </script>
    <script src="{{ url('panel/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{url('panel/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{url('panel/assets/pages/jquery.form-pickers.init.js')}}"></script>
@stop