@extends('web.layouts.app')
@section('content')
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    <section id="allNewsSection" class="getME">


        <div class="container filter-tabs">

            <ul class="nav nav-tabs">
                <li class="fadeInRight wow animated" data-wow-duration="1s" data-wow-delay="0.2s"><a data-toggle="tab" href="#NETWORK">@lang('alert.NetWorkTest')</a></li>
                <li class="fadeInRight wow animated" data-wow-duration="1s" data-wow-delay="0.4s"><a data-toggle="tab" href="#WEB"> @lang('alert.webDev')</a></li>
                <li class="fadeInRight wow animated" data-wow-duration="1s" data-wow-delay="0.6s"><a data-toggle="tab" href="#MOBILE">@lang('alert.AppDev')</a></li>
            </ul>
        </div>


        <div class="tab-content">
            <div class="container tab-pane fade in active" id="MOBILE">
                <div class="row">
                    @foreach($mobile_products as $mobile_product)
                        @php
                            $image=App\Models\ProductImage::where('product_id',$mobile_product->id)->value('name');
                            $title=App\Models\ProductDescription::where(['product_id'=>$mobile_product->id,'language_id'=>$language_id])->value('title');
                        @endphp
                        <div class="col-sm-4 col-xs-6 new-box-container fadeInRight animated wow" data-wow-duration="1s">
                            <div class="new-box">
                                <img src="{{asset('images/product/'.$image)}}" />
                                <div class="new-overlay">
                                    <h3 class="news-title aniamted wow">
                                        <a href="{{route('Project',$mobile_product->id)}}" class="hvr-underline-from-center">
                                            {{$title}}
                                        </a>
                                    </h3>
                                    <div class="news-eye aniamted wow">
                                        <a href="{{route('Project',$mobile_product->id)}}" class="hvr-ripple-out">
                                            <i class="ion-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="container tab-pane fade" id="WEB">
                <div class="row">
                    @foreach($web_products as $web_product)
                        @php
                            $image=App\Models\ProductImage::where('product_id',$web_product->id)->value('name');
                            $title=App\Models\ProductDescription::where(['product_id'=>$web_product->id,'language_id'=>$language_id])->value('title');
                        @endphp
                        <div class="col-sm-4 col-xs-6 new-box-container fadeInRight animated wow" data-wow-duration="1s">
                            <div class="new-box">
                                <img src="{{asset('images/product/'.$image)}}" />
                                <div class="new-overlay">
                                    <h3 class="news-title aniamted wow">
                                        <a href="#" class="hvr-underline-from-center">
                                            {{$title}}
                                        </a>
                                    </h3>
                                    <div class="news-eye aniamted wow">
                                        <a href="{{route('Project',$web_product->id)}}" class="hvr-ripple-out">
                                            <i class="ion-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>

            <div class="container tab-pane fade" id="NETWORK">
                <div class="row">
                    @foreach($networking_products as $networking_product)
                        @php
                            $image=App\Models\ProductImage::where('product_id',$networking_product->id)->value('name');
                            $title=App\Models\ProductDescription::where(['product_id'=>$networking_product->id,'language_id'=>$language_id])->value('title');
                        @endphp
                        <div class="col-sm-4 col-xs-6 new-box-container fadeInRight animated wow" data-wow-duration="1s">
                            <div class="new-box">
                                <img src="{{asset('images/product/'.$image)}}" />
                                <div class="new-overlay">
                                    <h3 class="news-title aniamted wow">
                                        <a href="#" class="hvr-underline-from-center">
                                            {{$title}}
                                        </a>
                                    </h3>
                                    <div class="news-eye aniamted wow">
                                        <a href="{{route('Project',$networking_product->id)}}" class="hvr-ripple-out">
                                            <i class="ion-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

                </div>
                {{--<div class="row">--}}
                    {{--<div class="col-xs-12 pagination-container">--}}
                        {{--<ul class="pagination">--}}
                            {{--<li class="animated wow">--}}
                                {{--<a href="#">--}}
                                    {{--<i class="ion-ios-arrow-right"></i>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="animated wow"><a href="#">1</a></li>--}}
                            {{--<li class="active animated wow"><a href="#">2</a></li>--}}
                            {{--<li class="animated wow"><a href="#">3</a></li>--}}
                            {{--<li class="animated wow"><a href="#">4</a></li>--}}
                            {{--<li class="animated wow"><a href="#">5</a></li>--}}
                            {{--<li class="animated wow">--}}
                                {{--<a href="#">--}}
                                    {{--<i class="ion-ios-arrow-left"></i>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>


    @stop