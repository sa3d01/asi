@extends('web.layouts.app')

@section('content')

    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    <section id="allNewsSection" class="getME">
        <div class="container">
            <div class="row">
                @foreach($news as $new)
                <div class="col-sm-4 col-xs-6 new-box-container fadeInRight animated wow" data-wow-duration="1s">
                    <div class="new-box">
                        <img src="{{asset('images/article/'.$new->image)}}"/>
                        <div class="new-overlay">
                            <h3 class="news-title aniamted wow">
                                <a href="{{route('Single.New',$new->id)}}" class="hvr-underline-from-center">
                                    {{App\Models\ArticleDescription::where(['language_id'=>$language_id,'article_id'=>$new->id])->first()->title}}
                                </a>
                            </h3>
                            <div class="news-eye aniamted wow">
                                <a href="{{route('Single.New',$new->id)}}" class="hvr-ripple-out">
                                    <i class="ion-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                @endforeach
            </div>
            {{--<div class="row">--}}
                {{--<div class="col-xs-12 pagination-container">--}}
                    {{--<ul class="pagination">--}}
                        {{--<li class="animated wow">--}}
                            {{--<a href="#">--}}
                                {{--<i class="ion-ios-arrow-right"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="animated wow"><a href="#">1</a></li>--}}
                        {{--<li class="active animated wow"><a href="#">2</a></li>--}}
                        {{--<li class="animated wow"><a href="#">3</a></li>--}}
                        {{--<li class="animated wow"><a href="#">4</a></li>--}}
                        {{--<li class="animated wow"><a href="#">5</a></li>--}}
                        {{--<li class="animated wow">--}}
                            {{--<a href="#">--}}
                                {{--<i class="ion-ios-arrow-left"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>


@stop