<footer id="footer" data-parallax="scroll" data-image-src="{{asset('web/images/footer.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <div class="contact">
                    <a href="#"><img src="{{asset('web/images/logo.png')}}"></a>
                    <br>
                    <div class="text">
                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو دد الحروف التى يولدها التطبيق.
                    </div><!-- text -->
                    <br>
                    <p class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </p>
                </div>
            </div><!-- m4 -->


            <div class="col-md-3 col-xs-12">

                <h4 class="title">الأقسام</h4>

                <ul>
                    <li><a href="#">الرئيسية</a></li>
                    <li><a href="#">من نحن</a></li>
                    <li><a href="#">العقارات</a></li>
                    <li><a href="#">اتصل بنا</a></li>
                </ul>

            </div><!-- m2 -->


            <div class="col-md-3 col-xs-12">
                <div class="contact">
                    <h4 class="title">اتصل بنا</h4>
                    <p><i class="fa fa-phone"></i> <span> <b>+966</b> 123456789</span></p>
                    <p><i class="fa fa-whatsapp"></i> <span> <b>+966</b> 123456789</span></p>
                    <p><i class="fa fa-envelope"></i> ahmed@gmail.com</p>
                </div>
            </div><!-- m4 -->
        </div><!-- row -->

        <div class="after_footer">
            <div class="wow fadeInUp" data-wow-duration="1.25s" data-wow-delay=".5s">
                جميع الحقوق محفوظة لموقع <span>ثمار التطور</span>
            </div><!-- md-6 -->

        </div><!-- after_footer -->


    </div><!-- container -->
</footer>
