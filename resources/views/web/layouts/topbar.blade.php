<div class="logo_nav nav-1">
    <div class="container">
        <div class="logo col-md-4">
            <a href="{{route('index')}}"><img src="{{asset('web/images/logo.png')}}"></a>
        </div><!-- logo -->

        <div class="col-sm-6">
            <div class="nav">
                <ul class="navbar_items">
                    <li class="current-page-item"><a href="{{route('index')}}">الرئيسيه</a></li>
                    <li><a href="#"> عقارات</a></li>
                    <li>
                        <a href="{{route('about')}}">من نحن </a>
                        {{--<a href="#">من نحن </a>--}}
                        {{--<ul class="sub-menu">--}}
                            {{--<li><a href="#"> PRODUCTS </a></li>--}}
                            {{--<li><a href="#">PORTFOLIO </a></li>--}}
                            {{--<li><a href="#">CLIENTS</a></li>--}}
                            {{--<li><a href="#">CONTACT US</a></li>--}}
                        {{--</ul><!-- dropdown -->--}}
                    </li>
                    <li><a href="#"> اتصل بنا </a></li>
                    <li class="search-click"> <i class="fa fa-search"></i></li>
                </ul><!-- navbar-items -->

                <div class="search-box">
                    <form action="">
                        <input type="text" placeholder="ابحث">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>


            </div><!-- nav -->
        </div><!-- sm-6 -->

        <div class="col-sm-2">
            <div class="lang">
                <a href="#" class="en-lnag">ENGLISH</a>
                <a href="#" class="ar-lang">عربي</a>
            </div><!-- lang -->
        </div>


        <div id="nav-icon1"  class="btn-responsive btn-r" style="display: none;">
            <span></span>
            <span></span>
            <span></span>
        </div>


    </div><!-- container -->
</div><!-- logo_nav -->

<div class="logo_nav nav-2">
    <div class="container">
        <div class="logo col-md-3 col-xs-7">
            <a href="#"><img src="{{asset('web/images/logo.png')}}"></a>
        </div><!-- logo -->

        <div class="col-sx-2 col-xs-5 icon-fixed-menu">

            <div id="nav-icon1"  class="btn-responsive">
                <span></span>
                <span></span>
                <span></span>
            </div>

        </div><!-- right -->
    </div><!-- container -->
</div><!-- logo_nav -->

<div id="myNav" class="overlay">
    <div class="overlay-content">
        <ul class="list-unstyled">
            <li class="current-page-item"><a href="#">الرئيسيه</a></li>
            <li><a href="#"> عقارات</a></li>
            <li>
                <a href="{{route('about')}}">من نحن </a>
                {{--<ul class="sub-menu">--}}
                    {{--<li><a href="#"> PRODUCTS </a></li>--}}
                    {{--<li><a href="#">PORTFOLIO </a></li>--}}
                    {{--<li><a href="#">CLIENTS</a></li>--}}
                    {{--<li><a href="#">CONTACT US</a></li>--}}
                {{--</ul><!-- dropdown -->--}}
            </li>
            <li><a href="#"> اتصل بنا </a></li>
        </ul>
    </div>
</div>