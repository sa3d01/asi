


<div class="slider-top">
    <div class="item">
        <img src="{{asset('web/images/bread.jpg')}}">
        <div class="overlay">
            <h3><p>Themar <span>Evolution</span></p></h3>
            <div class="text">Renting all kinds of properties for owners and knowing the details
                of the property and its condition</div>
        </div>
    </div>

    <div class="item">
        <img src="{{asset('web/images/2.png')}}">
        <div class="overlay">
            <h3><p>Themar <span>Evolution</span></p></h3>
            <div class="text">Renting all kinds of properties for owners and knowing the details
                of the property and its condition</div>
        </div>
    </div>

    <div class="item">
        <img src="{{asset('web/images/3.png')}}">
        <div class="overlay">
            <h3><p>Themar <span>Evolution</span></p></h3>
            <div class="text">Renting all kinds of properties for owners and knowing the details
                of the property and its condition</div>
        </div>
    </div>
</div>
<div class="social-media-header">
    <a href="#" data-toggle="tooltip" data-placement="right" title="facebook"><i class="fa fa-facebook"></i></a>
    <a href="#" data-toggle="tooltip" data-placement="right" title="twitter"><i class="fa fa-twitter"></i></a>
    <a href="#" data-toggle="tooltip" data-placement="right" title="Gmail"><i class="fa fa-google-plus"></i></a>
    <a href="#" data-toggle="tooltip" data-placement="right" title="Instagram"><i class="fa fa-instagram"></i></a>
</div>


<div class="page_options">
    <p data-toggle="tooltip" data-placement="left" title="الاعدادات"><i class="fa fa-cog"></i></p>
    <ul>
        <li><a href="">تسجيل دخول</a></li>
        <li><a href="">تسجيل جديد</a></li>
        <li><a href="">الاعدادات</a></li>
        <li><a href="#">خروج</a></li>
    </ul>
</div>