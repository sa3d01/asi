<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ASI لحلول البرمجة وبرمجة الأشياء </title>
    <meta name="description"
          content="شركة ASI | استضافة المواقع | استضافة الريسيلر | الشبكات | برمجة الأشياء | تصميم الواجهات | الدعم الفنى | تطبيقات الجوالات | تطبيقات الأندرويد | تطبيقات الأيفون | ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset('web/img/favicon.png')}}">
    <!--        Bootstrap-->
    <link rel="stylesheet" href="{{asset('web/css/bootstrap.min.css')}}"/>
    @if(\App::isLocale('ar'))
        <link rel="stylesheet" href="{{asset('web/css/bootstrap-rtl.min.css')}}"/>
    @endif

    {{--<link rel="stylesheet" href="{{asset('web/css/bootstrap-rtl.min.css')}}"/>--}}
            

<!--    OWL Carousel-->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.carousel.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.theme.default.min.css"/>
    <!--    Animate-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
    <!--        Hover-->
    <link rel="stylesheet" href="{{asset('web/css/hover-min.css')}}"/>
    <!--        Ionic-->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css"/>
    <!--        Search-->
    <link rel="stylesheet" href="{{asset('web/css/demo.css')}}"/>
    @yield('stackcss')
            @if(\App::isLocale('ar'))

<link rel="stylesheet" href="{{asset('web/css/style9.css')}}"/>                   
                        @else
<link rel="stylesheet" href="{{asset('web/css/style9ltr.css')}}"/>                    
                        
                        @endif
    <!--        SideBar-->
    <link rel="stylesheet" href="{{asset('web/css/component.css')}}"/>
    <link rel="stylesheet" href="{{asset('web/css/normalize.css')}}"/>
    <!--    Book Block-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('web/css/bookblock.css')}}"/> --}}
    <!--        Main Style-->
        @if(\App::isLocale('ar'))

<link rel="stylesheet" href="{{asset('web/css/main.css')}}"/>                    
                        @else
<link rel="stylesheet" href="{{asset('web/css/mainltr.css')}}"/>                    
                        
                        @endif
    
    <!--        Modernizer-->
    <script src="{{asset('web/js/snap.svg-min.js')}}"></script>
    <script src="{{asset('web/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    <script>
        document.documentElement.className = 'js';
    </script>
    @yield('style')
</head>
@if(\App::isLocale('ar'))
<body>
    @else
    <body class="leftened-body">
    @endif
<!-- loading -->
<div class="preloading-demo">
    <div id="fiSide">
        <div class="loader">
            <div class="loader-inner-1"></div>
            <div class="loader-inner-2"></div>
        </div>
    </div>
    <div id="seSide"></div>
</div>
<div id="st-container" class="st-container">
    <div class="st-pusher">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
                href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!--        ***** ----- +++++ Begin First Section +++++ ----- ***** -->
        <svg class="hidden">
            <defs>
                <symbol id="icon-cross" viewBox="0 0 24 24">
                    <title>cross</title>
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </symbol>
            </defs>
        </svg>
        @php
            $face=App\Models\Social::find(1);
            $inst=App\Models\Social::find(3);
            $twitter=App\Models\Social::find(2);
            $linked=App\Models\Social::find(4);
            $setting=App\Models\Setting::first();
        @endphp
        <section class="main-wrap section top-short-nav" id="sec1">
            <div class="most-top-header up-particles">
                <div class="container">
                    <div class="row up-particles contact-social">
                        <div class="col-sm-8 col-xs-12 text-right contacts">
                            <a href="mailto:contact@asi.com">
                                {{$setting->email}}
                                <i class="ion-email"></i>
                            </a>
                            <a href="tel:+201014963926" class="moby-num">
                                {{$setting->mobile_1}}+
                                <i class="ion-android-call"></i>
                            </a>
                        </div>
                        <div class="col-sm-4 xs-hidden text-left social-media hidden-xs">
                            <a href="{{$face->link}}">
                                <i class="ion-social-facebook"></i>
                            </a>
                            <a href="{{$twitter->link}}">
                                <i class="ion-social-twitter"></i>
                            </a>
                            <a href="{{$linked->link}}">
                                <i class="ion-social-linkedin"></i>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row up-particles navigation-bar-holder">
                        <div class="container up-particles">
                            <div id="st-trigger-effects" class="col-xs-2 visible-xs hamMenu-container">
                                <button class="burger-menu" data-effect="st-effect-12">
                                    <i class="ion-grid"></i>
                                </button>
                            </div>
                            <div class="col-sm-2 col-xs-8 logo-container">
                                <a href=""><img src="{{asset('web/img/logo1.png')}}"/></a>
                            </div>
                            <div class="col-sm-8 nav-links hidden-xs">
                                <nav>
                                    <a href="{{route('index')}}" class="{{ Request::path() == '/' ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::path() == '/' ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.home')
                                        <i class="{{ Request::path() == '/' ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                    <a href="{{route('Projects')}}" class="{{ Request::is('projects') ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::is('projects') ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.ourProjects')
                                        <i class="{{ Request::is('projects') ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                    <a href="{{route('HOST')}}" class="{{ Request::is('hosting') ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::is('hosting')  ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.Host')
                                        <i class="{{ Request::is('hosting')  ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                    <a href="{{route('NEWS')}}" class="{{ Request::is('news') ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::is('news') ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.News')
                                        <i class="{{ Request::is('news') ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                    <a href="{{route('Jobs')}}" class="{{ Request::is('jobs') ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::is('jobs') ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.jobs')
                                        <i class="{{ Request::is('jobs') ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                    <a href="{{route('Contact_Us')}}" class="{{ Request::is('contactUs') ? 'active' : 'hvr-overline-from-center' }}">
                                        <i class="{{ Request::is('contactUs') ? 'ion-network before-icc' : '' }}"></i>
                                        @lang('alert.ContactUs')
                                        <i class="{{ Request::is('contactUs') ? 'ion-network after-icc' : '' }}"></i>
                                    </a>
                                       @if(\App::isLocale('ar'))

                <a href="{{route('CHANGE_LANGUAGE','en')}}" class="hvr-overline-from-center">
                    English
                </a>
            @else
                <a href="{{route('CHANGE_LANGUAGE','ar')}}" class="hvr-overline-from-center">
                    العربيه
                </a>

            @endif
                                        <!--<a href="{{url('lang/en')}}" class="hvr-overline-from-center">-->
                                        <!--    English-->
                                        <!--</a>-->
                                </nav>
                            </div>
                            <div class="col-xs-2 up-particles search-icon-container">
                                <a href="#" id="btn-search">
                                    <i class="ion-android-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--            Middle content text-->
                    <div class="row">
                        <div class="col-xs-12  welcome-text-container">
                            <h3>@lang('alert.welcome')<span class="colored-text">ASI</span>!</h3>
                            <div class="typing-container">
                                <span id="itypedOne"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 scroll-to-bottom-arrow">
                            <a href="#" class="down hov-arrow">
                                <i class="ion-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{--<canvas class="background"></canvas>--}}
            <div class="overlay vLight"></div>
        </section>
        <div class="search up-particles">
            <button id="btn-search-close" class="btn btn--search-close" aria-label="Close search form">
                <svg class="icon icon--cross">
                    <use xlink:href="#icon-cross"></use>
                </svg>
            </button>
            <div class="search__inner search__inner--up">


                {!! Form::open(['method'=>'post','route'=>['search'],'class'=>'search__form']) !!}
                    <input class="search__input" name="search" type="search" placeholder="@lang('alert.search')"
                           autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                    <span class="search__info">@lang('alert.searchDet')</span>
                {!! Form::close() !!}




            </div>
            <div class="search__inner search__inner--down">
                <div class="search__related">
                    <div class="search__suggestion">
                        <h3>@lang('alert.suggest1')</h3>
                        <p>#لابتوب # تقنية # جوجل #ناسا #الانترنت #واى فاى #فيسبوك #التقنية #أخبار #أندرويد #أبل</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        </div>-->
    <nav class="st-menu st-effect-12" id="menu-12">
        <a href="/"><img src="{{asset('web/img/logo1.png')}}"/></a>
        <ul>
            <li><a href="{{route('index')}}" class="{{ Request::is('') ? 'active' : 'hvr-overline-from-center' }}">                                        @lang('alert.home')
                </a></li>
                <li><a href="{{route('Projects')}}"
                   class="{{ Request::is('Projects') ? 'active' : 'hvr-overline-from-center' }}">                                        @lang('alert.ourProjects')
                </a></li>
            <li><a href="{{route('HOST')}}" class="{{ Request::is('HOST') ? 'active' : 'hvr-overline-from-center' }}">@lang('alert.Host')</a></li>
            
            <li><a href="{{route('NEWS')}}"
                   class="{{ Request::is('NEWS') ? 'active' : 'hvr-overline-from-center' }}">                                        @lang('alert.News')
                </a></li>
            <li><a href="{{route('Jobs')}}"
                   class="{{ Request::is('Jobs') ? 'active' : 'hvr-overline-from-center' }}">                                        @lang('alert.jobs')
                </a></li>
            <li><a href="{{route('Contact_Us')}}"
                   class="{{ Request::is('Contact_Us') ? 'active' : 'hvr-overline-from-center' }}">                                        @lang('alert.ContactUs')
                </a></li>
            @if(\App::isLocale('ar'))
            <li>
                <a href="{{route('CHANGE_LANGUAGE','en')}}" class="hvr-overline-from-center">
                    English
                </a>
            </li>
            @else
            <li>
                <a href="{{route('CHANGE_LANGUAGE','ar')}}" class="hvr-overline-from-center">
                    العربيه
                </a>
                </li>
            @endif
        </ul>
    </nav>
</div>
<!-- ======================== -->
<!-- end slider -->

@yield('content')
<!-- start footer -->
@php
    $face=App\Models\Social::find(1);
    $inst=App\Models\Social::find(3);
    $twitter=App\Models\Social::find(2);
    $linked=App\Models\Social::find(4);
@endphp
<section class="section" id="secFooter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 socials-container">
                <a target="_blank" href="{{$face->link}}" class="soc-icon hvr-ripple-out  animated wow fadeInRight" data-wow-duration="1s"
                   data-wow-delay="0s">
                    <i class="ion-social-facebook"></i>
                </a>
                <a target="_blank" href="{{$twitter->link}}" class="soc-icon hvr-ripple-out  animated wow fadeInRight" data-wow-duration="1s"
                   data-wow-delay="0.1s">
                    <i class="ion-social-twitter"></i>
                </a>
                <a target="_blank" href="{{$inst->link}}" class="soc-icon hvr-ripple-out  animated wow fadeInRight" data-wow-duration="1s"
                   data-wow-delay="0.2s">
                    <i class="ion-social-instagram-outline"></i>
                </a>
                <a target="_blank" href="{{$linked->link}}" class="soc-icon hvr-ripple-out  animated wow fadeInRight" data-wow-duration="1s"
                   data-wow-delay="0.3s">
                    <i class="ion-social-linkedin-outline"></i>
                </a>
            </div>
            <div class="col-xs-12 rights-reserved">
                @lang('alert.rights') &copy; 2017 <span> ASI </span>
            </div>
        </div>
    </div>
</section>
<!--        ***** ----- +++++ End Partners Section +++++ ----- ***** -->
<!--        ***** ----- +++++ Begin TOP BTN Section +++++ ----- ***** -->
<a href="#" id="back-to-top">
    <i class="ion-android-arrow-up"></i>
</a>
<!--        ***** ----- +++++ End TOP BTN Section +++++ ----- ***** -->
<!--        ***** ----- +++++ Begin Scripts Section +++++ ----- ***** -->
<!--        jQuery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@yield('isvisible')
<!--        Bootstrap-->
<script src="{{asset('web/js/vendor/bootstrap.min.js')}}"></script>
<!--        Particles-->
<script src="{{asset('web/js/particles.min.js')}}"></script>
<script src="{{asset('web/js/particlesinit.js')}}"></script>
<!--    Typed-->
<script src="{{asset('web/js/typed.js')}}"></script>
<script>
    ityped.init("#itypedOne", {
        strings: ['@lang('alert.welcome2')']
    })
</script>
<!--    OWL Carousel-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/owl.carousel.min.js"></script>
<script src="{{asset('web/js/owlinit.js')}}"></script>
<!--        Search-->
<script src="{{asset('web/js/demo9.js')}}"></script>
<!--        SideBar-->
<script src="{{asset('web/js/classie.js')}}"></script>
<script src="{{asset('web/js/sidebarEffects.js')}}"></script>
<!--    Icon Animation-->
<script src="{{asset('web/js/jquery.drawsvg.js')}}"></script>
<!--    WOW js-->
<script src="{{asset('web/js/wow.js')}}"></script>
<script src="{{asset('web/js/wowinit.js')}}"></script>
<!--    Nice Scroll-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<!--    Book Block-->
{{-- <script src="{{asset('web/js/jquery.bookblock.min.js')}}"></script>
<script src="{{asset('web/js/bookblockinit.js')}}"></script>
<script>
    Page.init();
</script> --}}
<!--        Main Script-->
@yield('scripts')
<script src="{{asset('web/js/main.js')}}"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--<script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>-->
<!--        ***** ----- +++++ End Scripts Section +++++ ----- ***** -->

@yield('script')
</body>
</html>