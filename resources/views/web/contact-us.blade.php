@extends('web.layouts.app')
@section('content')
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    <section id="allNewsSection" class="getME">
        <div class="container">
            <div class="row ">
                <div class="col-sm-6 col-xs-12 contact-form-container">
                    <form method="post" action="{{route('POST_SEND_CONTACT')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="text" placeholder="@lang('alert.name')" name="name" class="wow animated fadeInRight"
                               data-wow-duration="1s" data-wow-delay="0s"/>
                        <input type="text" placeholder="@lang('alert.email')" name="email" class="wow animated fadeInRight"
                               data-wow-duration="0.9s" data-wow-delay="0.1s"/>
                        <input type="text" name="phone" placeholder="@lang('alert.phone') " class="wow animated fadeInRight"
                               data-wow-duration="0.8s" data-wow-delay="0.2s"/>
                        <textarea placeholder="@lang('alert.message')" name="message" class="wow animated fadeInRight" data-wow-duration="0.7s"
                                  data-wow-delay="0.3s"></textarea>
                        <button type="submit" class="hvr-sweep-to-left wow animated fadeInRight"
                                data-wow-duration="0.6s" data-wow-delay="0.4s">@lang('alert.send')
                        </button>
                    </form>
                </div>
                <div class="col-sm-6 col-xs-12 wow animated fadeInLeft" data-wow-duration="1s">
                    <div class="map-wrap">
                        <div id="map" class="google-map"></div>
                    </div>
                </div>
            </div>
            <div class="row contacting-info-wrapper">
                <div class="col-xs-4 contact-item fade wow animated fadeInRight" data-wow-duration="1s"
                     data-wow-delay="0.1s">
                    <div class="contact-item-icon">
                        <img src="{{asset('web/img/phone-call.png')}}"/>
                    </div>
                    <div class="contact-item-text">
                        <div class="contact-item-label">@lang('alert.phone'):</div>
                        <div class="contact-item-info">{{$setting->mobile_1}}</div>
                    </div>
                </div>
                <div class="col-xs-4 contact-item fade wow animated fadeInRight" data-wow-duration="1s"
                     data-wow-delay="0.2s">
                    <div class="contact-item-icon">
                        <img src="{{asset('web/img/email.png')}}"/>
                    </div>
                    <div class="contact-item-text">
                        <div class="contact-item-label">@lang('alert.email'):</div>
                        <div class="contact-item-info">{{$setting->email}}</div>
                    </div>
                </div>
                <div class="col-xs-4 contact-item fade wow animated fadeInRight" data-wow-duration="1s"
                     data-wow-delay="0.3s">
                    <div class="contact-item-icon">
                        <img src="{{asset('web/img/map-location.png')}}"/>
                    </div>
                    <div class="contact-item-text">
                        <div class="contact-item-label">@lang('alert.address') :</div>
                        <div class="contact-item-info">{{App\Models\SettingDescription::where(['language_id'=>$language_id,'setting_id'=>1])->value('address')}}</div>
                    </div>
                </div>
            </div>
            <div class="row all-deaprtments-container">
                <div class="col-xs-12">
                    <h3 class="subsec-title">@lang('alert.branches')</h3>
                </div>


                {{--Foreach for الفروع--}}
                @foreach($branches as $branch)
                <div class="col-sm-4 col-xs-12 department-info-container">
                    <div class="department-photo wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0s">
                        <a href="{{asset('images/branche/'.$branch->image)}}" data-fancybox="dept1">
                            <img src="{{asset('images/branche/'.$branch->image)}}"/>
                        </a>
                    </div>
                    <div class="dept-location wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="1.1s">
                        <i class="ion-location"></i> {{App\Models\BrancheDescription::where(['branche_id'=>$branch->id,'language_id'=>$language_id])->value('address')}}
                    </div>
                    <div class="dept-mail wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="1.2s">
                        <i class="ion-email"></i>
                        <a href="mailto:promursi@gmail.com">{{$branch->email}}</a>
                    </div>
                    <div class="dept-phone wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="1.3s">
                        <i class="ion-android-call"></i>
                        <a href="tel:+{{$branch->mobile}}">{{$branch->mobile}}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>


@stop
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--        Bootstrap-->
    <script src="{{asset('web/js/vendor/bootstrap.min.js')}}"></script>



    <!--    Typed-->
    {{--<script src="{{asset('web/js/typed.js')}}"></script>--}}
    {{--<script>--}}
        {{--ityped.init("#itypedOne" , {--}}
            {{--strings: [' لدينا فريق دعم فنى على مدار 24 ساعة', 'قم بالتواصل معنا الأن واحصل على خدمة فريدة من نوعها  ...']--}}
        {{--})--}}
    {{--</script>--}}
    <!--    Nice Scroll-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <!--        Search-->
    <script src="{{asset('web/js/demo9.js')}}"></script>
    <!--    Fancy Box-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <!--        SideBar-->
    <script src="{{asset('web/js/classie.js')}}"></script>
    <script src="{{asset('web/js/sidebarEffects.js')}}"></script>
    <!--    Google Maps -->
    <script src="{{asset('web/js/googlemap.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvNC8zclbwzdQMM-XUMA16M0qGe7x0UV0&callback=myMap"></script>
    <!--    WOW js-->
    <script src="{{asset('web/js/wow.js')}}"></script>
    <script src="{{asset('web/js/wowinit.js')}}"></script>
    <!--        Main Script-->
    <script src="{{asset('web/js/main.js')}}"></script>
    @stop