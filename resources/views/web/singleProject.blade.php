@extends('web.layouts.app')
@section('stackcss')
<link rel="stylesheet" href="{{asset('web/css/style.css')}}"/>                   
@endsection
@section('content')
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    @php
        $title=App\Models\ProductDescription::where(['product_id'=>$product->id,'language_id'=>$language_id])->value('title');
        $note=App\Models\ProductDescription::where(['product_id'=>$product->id,'language_id'=>$language_id])->value('note');
        $images=App\Models\ProductImage::where(['product_id'=>$product->id])->get();
    @endphp
    <section id="projInnerSection" class="getME">
        <div class="container">
            @if($product->video)
            <div class="row">
                <div class="col-xs-12">
                    <div id='player'>
                        <div class="video-wrapper">
                            <video id='video-element' poster="{{asset('img/poster1.PNG')}}">
                                <source src='{{asset('videos/product/'.$product->video)}}' type='video/mp4'>
                                <source src='{{asset('videos/product/'.$product->video)}}' type='video/ogg'>
                            </video>
                            <div id='controls'>
                                <progress id='progress-bar' min='0' max='100' value='0'>0% played</progress>
                                <button id='btnPlayPause' class='play' title='play' accesskey="P" onclick='playPauseVideo();'>Play</button>
                                <input type="range" id="volume-bar" title="volume" min="0" max="1" step="0.1" value="1">
                                <button id='btnMute' class='mute' title='mute' onclick='muteVolume();'>Mute</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12 inner-project-description">
                    <h3 class="project-title">
                        {{$title}}
                    </h3>
                    <div>
                       {!! $note !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <ul id="st-stack" class="st-stack-raw">
                        @foreach($images as $image)
                        <li>
                            <div class="st-item">
                                <div class="work-image-container">
                                    <div class="work-image">
                                        <a class="fancy-box" data-fancybox="gallery" href="{{asset('images/product/'.$image->name)}}">
                                            <img src="{{asset('images/product/'.$image->name)}}" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="st-title">
                                <h2>{{$title}}</h2>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-12 stores-container">
                    @if($product->android)
                    <div class="col-xs-6 google-store animated wow fadeInRight" data-wow-delay="0s" data-wow-duration="1s">
                        <a href="{{$product->android}}" class="hvr-float-shadow">
                            <img src="{{asset('web/img/google.png')}}" />
                        </a>
                    </div>
                    @endif
                    @if($product->ios)
                    <div class="col-xs-6 apple-store animated wow fadeInLeft" data-wow-delay="0s" data-wow-duration="1s">
                        <a href="{{$product->ios}}" class="hvr-float-shadow ">
                            <img src="{{asset('web/img/apple.png')}}" />
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@stop
@section('script')
<script src="{{asset('web/js/classie.js')}}"></script>
    <script src="{{asset('web/js/sidebarEffects.js')}}"></script>
    <!--    Stack Slider-->
    <script src="{{asset('web/js/jquery.stackslider.js')}}"></script>
    
     <script type="text/javascript">
        $( function() {
        				$( '#st-stack' ).stackslider();
        			});
    </script>
    <script src="{{asset('web/js/videoplayer.js')}}"></script>
@stop