@extends('web.layouts.app')
@if (session()->has('language'))
    @if (session()->get('language') == 'en')
        <?php $language_id=2;?>
    @else
        <?php $language_id=1;?>
    @endif
@else
    <?php $language_id=1;?>
@endif
@section('content')

    <section id="allNewsSection" class="getME">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 available-jobs">
                <h3 class="the-title animated fadeInDown wow" data-wow-duration="1s">
                    @lang('alert.AvailJobs')
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="panel-group" id="accordion">
@foreach($jops as $jop)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$jop->id}}">{{App\Models\JopDescription::where(['language_id'=>$language_id,'jop_id'=>$jop->id])->value('title')}}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$jop->id}}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 job-image wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="0s">
                                <img src="{{asset('images/jop/'.$jop->image)}}" title="{{App\Models\JopDescription::where(['language_id'=>$language_id,'jop_id'=>$jop->id])->value('title')}}" />
                            </div>

                            <div class="col-xs-12 job-description  wow  fadeInDown animated" data-wow-duration="1s" data-wow-delay="1s">
                                <div class="job-subtitle">
                                    {{trans('alert.job_desc')}} :
                                </div>
                                {!!App\Models\JopDescription::where(['language_id'=>$language_id,'jop_id'=>$jop->id])->value('note')!!}
                            </div>
                            <div class="col-xs-12  wow  fadeInDown animated" data-wow-duration="1s" data-wow-delay="2s" >
                                <div class="job-subtitle">
                                    {{trans('alert.job_need')}} :
                                </div>
                                {!!App\Models\JopDescription::where(['language_id'=>$language_id,'jop_id'=>$jop->id])->value('need')!!}
                            </div>

                            <div class="col-xs-12 apply-for-job">
                                <a href="{{route('Contact_Us')}}" class="hvr-sweep-to-left apply-to-job">@lang('alert.apply')</a>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
            </div>
        </div>
    </div>
</section>

    @stop