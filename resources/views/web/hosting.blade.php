@extends('web.layouts.app')

@section('content')


    <section id="allNewsSection" class="getME">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 available-jobs">
                    <h3 class="the-title animated fadeInDown wow" data-wow-duration="1s">
                        @lang('alert.Host')
                    </h3>
                </div>
            </div>
            <div class="row hosting-plans-wrapper">
                <div class="col-sm-4 col-xs-12 hosting-plan-container">
                    <div class="hosting-plan wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="baqa-photo">
                            <img src="{{asset('web/img/database.png')}}" title="@lang('alert.diamond')"/>
                        </div>
                        <div class="baqa-name">
                            @lang('alert.diamond')
                        </div>
                        <div class="baqa-price">
                            {{App\Models\Host::where('id','2')->first()->price}} $ / @lang('alert.month')
                        </div>
                        <div class="baqa-storage">
                            500 @lang('alert.giga')
                        </div>
                        <div class="baqa-domains">
                            @lang('alert.unlimited')
                        </div>
                        <div class="baqa-support">@lang('alert.support')</div>
                        <div class="baqa-button">
                            <a href="{{route('Contact_Us')}}"
                               class="hvr-sweep-to-left apply-to-job">@lang('alert.Buy')</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 hosting-plan-container">
                    <div class="hosting-plan wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s">
                        <div class="baqa-photo">
                            <img src="{{asset('web/img/database.png')}}" title="@lang('alert.gold')"/>
                        </div>
                        <div class="baqa-name">
                            @lang('alert.gold')
                        </div>
                        <div class="baqa-price">
                            {{App\Models\Host::where('id','1')->first()->price}}$ / @lang('alert.month')
                        </div>
                        <div class="baqa-storage">
                            200 @lang('alert.giga')
                        </div>
                        <div class="baqa-domains">
                            4 @lang('alert.freeDomain')
                        </div>
                        <div class="baqa-support"> @lang('alert.support')</div>
                        <div class="baqa-button">
                            <a href="{{route('Contact_Us')}}"
                               class="hvr-sweep-to-left apply-to-job">@lang('alert.Buy')</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 hosting-plan-container">
                    <div class="hosting-plan wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.4s">
                        <div class="baqa-photo">
                            <img src="{{asset('web/img/database.png')}}"
                                 title=" @lang('alert.silver')"/>
                        </div>
                        <div class="baqa-name">
                            @lang('alert.silver')
                        </div>
                        <div class="baqa-price">
                            {{App\Models\Host::where('id','3')->first()->price}}$ / @lang('alert.month')
                        </div>
                        <div class="baqa-storage">
                            100 @lang('alert.giga')
                        </div>
                        <div class="baqa-domains">
                            2 @lang('alert.freeDomain')
                        </div>
                        <div class="baqa-support"> @lang('alert.support')</div>
                        <div class="baqa-button">
                            <a href="{{route('Contact_Us')}}"
                               class="hvr-sweep-to-left apply-to-job">@lang('alert.Buy')</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hosting-advice-wrapper">
                <div class="col-sm-6 col-xs-12  wow animated fadeInRight" data-wow-duration="1s">
                    <div class="">
                        <h3 class="the-title">
                            @lang('alert.WhyUs')
                        </h3>
                        <div class="why-us-rigt-txt">
                    {!! \App\Models\SettingDescription::orderBy('created_at','DESC')->where('language_id',$language_id)->first()->about !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 wow animated fadeInLeft" data-wow-duration="1s">
                    <div class="server-video-container">
                        <div id='player'>
                            <div class="video-wrapper">
                                <video id='video-element' poster="{{asset('web/img/poster1.PNG')}}">
                                    <source src='{{asset('web/img/ASIAr.mp4')}}' type='video/mp4'>
                                    <source src='{{asset('web/img/ASIAr.ogg')}}' type='video/ogg'>
                                </video>
                                <div id='controls'>
                                    <progress id='progress-bar' min='0' max='100' value='0'>0% played</progress>
                                    <button id='btnPlayPause' class='play' title='play' accesskey="P"
                                            onclick='playPauseVideo();'>Play
                                    </button>
                                    <input type="range" id="volume-bar" title="volume" min="0" max="1" step="0.1"
                                           value="1">
                                    <button id='btnMute' class='mute' title='mute' onclick='muteVolume();'>Mute</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($available))
                @if($available===true)
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <strong>{{$domain}}</strong><p>{{trans('alert.avilable')}}</p>
                    </div>
                @else
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <strong>{{$domain}}</strong><P>{{trans('alert.not_avilable')}}</P>
                    </div>
                @endif
            @endif
            <div class="row">
                <div class="col-sm-3 search-domain-title">
                    <h3 class="the-title">
                        @lang('alert.domainSearch')
                    </h3>
                </div>
                {!! Form::open(['method'=>'post','route'=>['domain'],'class'=>'col-sm-9 search-domain-form']) !!}
                    <input name="domain" type="text" class="search-domain-box" placeholder="example@example.com"/>
                    <button type="submit" class="search-domain-btn hvr-sweep-to-left">@lang('alert.search')</button>
                {!! Form::close() !!}
            </div>

        </div>
    </section>

@stop

@section('scripts')
    <script src="{{asset('web/js/videoplayer.js')}}"></script>
    <script src="{{asset('web/js/sidebarEffects.js')}}"></script>
@stop