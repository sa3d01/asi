@extends('web.layouts.app')
@section('content')
    <!--        ***** ----- +++++ End First Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Second Section +++++ ----- ***** -->
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
    <section class="section getME" id="sec2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-sm-8 col-xs-12 about-declaration-text-container ">
                        <div class="about-dec">
                            <h3 class="the-title">
                                @lang('alert.AboutUs')
                            </h3> 
                            <div class="right-declaration-text">
                                {!! \App\Models\SettingDescription::where(['language_id'=>$language_id,'setting_id'=>1])->value('about') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12 about-declaration-image-container ">
                        <img src="{{asset('web/img/1.png')}}" class="animated fadeInRight wow" data-wow-duration="1s"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Second Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Third Section +++++ ----- ***** -->
{{--     <section class="section" id="sec3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title animated fadeInDown wow" data-wow-duration="1s">
                        @lang('alert.workTeam')
                    </h3>
                    <div class="bb-custom-wrapper">
                        <div id="bb-bookblock" class="bb-bookblock">

                            @foreach($teams as $team)
                                <div class="bb-item">
                                    <img src="{{asset('images/team/'.$team->image)}}" alt="{{$team->name}}" width="400"
                                         height="300"/>
                                    <div class="employee-name">
                                        {{$team->name}}
                                    </div>
                                    <div class="employee-position">
                                        {!! \App\Models\TeamDescription::where(['language_id'=>$language_id,'team_id'=>$team->id])->value('job') !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <nav class="book-controls">
                            <a id="bb-nav-last" href="#" class="hov-arrow  animated fadeInRight wow"
                               data-wow-duration="1s" data-wow-delay="0s">
                                <i class="ion-ios-skipforward-outline"></i>
                            </a>
                            <a id="bb-nav-next" href="#" class="hov-arrow  animated fadeInRight wow"
                               data-wow-duration="1s" data-wow-delay="0.25s">
                                <i class="ion-ios-arrow-right"></i>
                            </a>
                            <a id="bb-nav-prev" href="#" class="hov-arrow  animated fadeInRight wow"
                               data-wow-duration="1s" data-wow-delay="0.5s">
                                <i class="ion-ios-arrow-left"></i>
                            </a>
                            <a id="bb-nav-first" href="#" class="hov-arrow  animated fadeInRight wow"
                               data-wow-duration="1s" data-wow-delay="0.75s">
                                <i class="ion-ios-skipbackward-outline"></i>
                            </a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!--        ***** ----- +++++ End Third Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Third Section +++++ ----- ***** -->
    <section class="section" id="sec4">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title">
                        @if(\App::isLocale('ar'))

إنجازاتنا                        
                        @else
                        Achievements
                        
                        @endif
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-timeline">

                        @foreach($acs as $ac)
                            <div class="timeline  animated fadeInRight wow" data-wow-duration="1s"
                                 data-wow-delay="0.75s">
                                <div class="timeline-content">
                                    <span class="year">{{$ac->date}}</span>
                                    <span class="post">{!! \App\Models\AchievementDescription::where(['language_id'=>$language_id,'achievement_id'=>$ac->id])->value('title')!!}</span>
                                    <h4 class="title">{{$ac->manager}}</h4>
                                    <p class="description">
                                        {!! \App\Models\AchievementDescription::where(['language_id'=>$language_id,'achievement_id'=>$ac->id])->value('note')!!}
                                    </p>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Fourth Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Fifth Section +++++ ----- ***** -->
    <section class="section" id="sec5">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title">
                        @lang('alert.exper')

                    </h3>
                </div>
                <div class="col-xs-12 advantages-wrapper">
                    <div class="col-xs-6 col-sm-3 advantage-container animated wow fadeInRight" data-wow-duration="1s"
                         data-wow-delay="0s">
                        <div class="advantage">
                            <div class="adv-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 363.868 363.868"
                                     style="enable-background:new 0 0 363.868 363.868;" xml:space="preserve"
                                     width="50px" height="50px"
                                     id="animatedSVG">
<g>
    <path d="M92.723,274.945c-3.178,3.178-5.747,9.388-5.747,13.875v58.444h33.929v-92.373   c0-4.487-2.569-5.56-5.747-2.382L92.723,274.945z"
          fill="#f7b82b"/>
    <path d="M241.752,219.573c-3.17,3.178-5.747,9.389-5.747,13.884v113.816h33.929V199.487   c0-4.487-2.569-5.552-5.747-2.374L241.752,219.573z"
          fill="#f7b82b"/>
    <path d="M291.418,169.834c-3.178,3.17-5.755,9.38-5.755,13.867v163.563h31.547V152.212   c0-4.487-2.577-5.56-5.755-2.382L291.418,169.834z"
          fill="#f7b82b"/>
    <path d="M193.078,268.239c0,0-1.512,1.52-3.381,3.39c-1.861,1.87-3.373,7.031-3.373,11.518v64.118h33.929   v-98.047c0-4.487-2.577-5.56-5.755-2.382L193.078,268.239z"
          fill="#f7b82b"/>
    <path d="M142.405,250.998c-3.178-3.17-5.755-2.105-5.755,2.382v93.885h33.929v-60.03   c0-4.487-2.439-10.559-5.454-13.558l-5.454-5.43L142.405,250.998z"
          fill="#f7b82b"/>
    <path d="M50.023,317.669l-10.957,10.974c-3.17,3.178-5.739,8.633-5.739,12.193v6.438h37.871V304.59   c0-4.487-2.569-5.552-5.747-2.374L50.023,317.669z"
          fill="#f7b82b"/>
    <path d="M358.121,150.724c3.17,3.178,5.747,2.105,5.747-2.382V32.193c0-8.316-7.966-15.599-16.233-15.599   H232.16c-4.487,0-5.56,2.577-2.382,5.755l41.074,41.106l-16.753,16.68l-77.701,77.774L135.3,116.82   c-3.178-3.178-8.316-3.17-11.494,0L9.519,231.189C-3.178,243.894-3.17,264.484,9.527,277.18l0.797,0.805   c12.697,12.697,33.287,12.697,45.975-0.008l73.247-73.287l41.098,41.057c3.178,3.17,8.324,3.17,11.502,0l135.479-135.503   L358.121,150.724z"
          fill="#f7b82b"/>
</g>
</svg>
                            </div>
                            <div class="adv-name">
                                @lang('alert.develop')
                            </div>
                            <div class="adv-desc">

                                {!! \App\Models\ExperienceDescription::where(['language_id'=>$language_id,'experience_id'=>'3'])->first()->note !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3 advantage-container animated wow fadeInRight" data-wow-duration="1s"
                         data-wow-delay="0.1s">
                        <div class="advantage">
                            <div class="adv-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" id="Capa_1" x="0px" y="0px" width="50px" height="50px"
                                     id="animatedSVG" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;"
                                     xml:space="preserve">
<g>
    <path d="M25.302,0H9.698c-1.3,0-2.364,1.063-2.364,2.364v30.271C7.334,33.936,8.398,35,9.698,35h15.604   c1.3,0,2.364-1.062,2.364-2.364V2.364C27.666,1.063,26.602,0,25.302,0z M15.004,1.704h4.992c0.158,0,0.286,0.128,0.286,0.287   c0,0.158-0.128,0.286-0.286,0.286h-4.992c-0.158,0-0.286-0.128-0.286-0.286C14.718,1.832,14.846,1.704,15.004,1.704z M17.5,33.818   c-0.653,0-1.182-0.529-1.182-1.183s0.529-1.182,1.182-1.182s1.182,0.528,1.182,1.182S18.153,33.818,17.5,33.818z M26.021,30.625   H8.979V3.749h17.042V30.625z"
          fill="#f7b82b"/>
</g>
</svg>
                            </div>
                            <div class="adv-name">
                                @lang('alert.MobileApp')
                            </div>
                            <div class="adv-desc">
                       
                           {!! \App\Models\ExperienceDescription::where(['language_id'=>$language_id,'experience_id'=>'4'])->first()->note!!}

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3 advantage-container animated wow fadeInRight" data-wow-duration="1s"
                         data-wow-delay="0.2s">
                        <div class="advantage">
                            <div class="adv-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" viewBox="0 0 297.23 297.23" enable-background="new 0 0 297.23 297.23"
                                     width="50px" height="50px" class="animatedSVG">
                                    <g>
                                        <path d="m149.416,61.02c14.139,0 25.642-11.503 25.642-25.642 0-14.139-11.503-25.642-25.642-25.642s-25.642,11.503-25.642,25.642c0,14.139 11.503,25.642 25.642,25.642z"
                                              fill="#f7b82b"/>
                                        <path d="m108.813,139.678h80.845c5.265,0 9.533-4.268 9.533-9.533v-35.25c0-9.758-6.271-18.41-15.544-21.448l-.043-.014-13.563-2.246c-1.154-0.355-2.388,0.256-2.803,1.395l-15.389,42.224c-0.888,2.436-4.333,2.436-5.221,0l-15.389-42.224c-0.335-0.92-1.203-1.496-2.133-1.496-0.22,0-0.445,0.033-0.667,0.101l-13.566,2.243c-9.349,3.115-15.595,11.782-15.595,21.582v35.133c0.002,5.265 4.27,9.533 9.535,9.533z"
                                              fill="#f7b82b"/>
                                        <path d="m50.188,208.836c14.139,0 25.642-11.503 25.642-25.642s-11.503-25.642-25.642-25.642c-14.139,0-25.642,11.503-25.642,25.642s11.503,25.642 25.642,25.642z"
                                              fill="#f7b82b"/>
                                        <path d="m84.368,221.262l-.043-.014-13.563-2.246c-1.154-0.355-2.388,0.256-2.803,1.395l-15.389,42.224c-0.888,2.436-4.333,2.436-5.221,0l-15.389-42.224c-0.335-0.92-1.203-1.496-2.133-1.496-0.22,0-0.445,0.033-0.667,0.101l-13.566,2.243c-9.348,3.115-15.594,11.782-15.594,21.582v35.133c0,5.265 4.268,9.533 9.533,9.533h80.845c5.265,0 9.533-4.268 9.533-9.533v-35.25c0-9.757-6.27-18.41-15.543-21.448z"
                                              fill="#f7b82b"/>
                                        <path d="m247.277,208.836c14.139,0 25.642-11.503 25.642-25.642s-11.503-25.642-25.642-25.642c-14.139,0-25.642,11.503-25.642,25.642s11.502,25.642 25.642,25.642z"
                                              fill="#f7b82b"/>
                                        <path d="m281.686,221.262l-.043-.014-13.563-2.246c-1.154-0.355-2.388,0.256-2.803,1.395l-15.389,42.224c-0.888,2.436-4.333,2.436-5.221,0l-15.389-42.224c-0.335-0.92-1.203-1.496-2.133-1.496-0.22,0-0.445,0.033-0.667,0.101l-13.566,2.243c-9.349,3.115-15.595,11.782-15.595,21.582v35.133c0,5.265 4.268,9.533 9.533,9.533h80.845c5.265,0 9.533-4.268 9.533-9.533v-35.25c0.002-9.757-6.269-18.41-15.542-21.448z"
                                              fill="#f7b82b"/>
                                        <path d="m157.872,146.894h-16.922v38.55l-39.834,39.834c3.606,4.936 5.679,10.989 5.679,17.431v0.822l42.616-42.617 40.975,40.976c0.205-6.527 2.528-12.62 6.417-17.515l-38.931-38.931v-38.55z"
                                              fill="#f7b82b"/>
                                        <path d="m155.539,71.055c-0.667-0.726-1.641-1.092-2.627-1.092h-7.353c-0.986,0-1.96,0.365-2.627,1.092-1.032,1.124-1.182,2.748-0.449,4.018l3.93,5.925-1.84,15.522 3.623,9.638c0.353,0.969 1.724,0.969 2.078,0l3.623-9.638-1.84-15.522 3.93-5.925c0.733-1.27 0.584-2.894-0.448-4.018z"
                                              fill="#f7b82b"/>
                                        <path d="m56.259,218.901c-0.667-0.726-1.641-1.092-2.627-1.092h-7.353c-0.986,0-1.96,0.365-2.627,1.092-1.032,1.124-1.182,2.748-0.449,4.018l3.93,5.925-1.84,15.521 3.623,9.638c0.353,0.969 1.724,0.969 2.077,0l3.623-9.638-1.84-15.521 3.93-5.925c0.734-1.269 0.585-2.893-0.447-4.018z"
                                              fill="#f7b82b"/>
                                        <path d="m253.577,218.901c-0.667-0.726-1.641-1.092-2.627-1.092h-7.353c-0.986,0-1.96,0.365-2.627,1.092-1.032,1.124-1.182,2.748-0.449,4.018l3.93,5.925-1.84,15.521 3.623,9.638c0.353,0.969 1.724,0.969 2.077,0l3.623-9.638-1.84-15.521 3.93-5.925c0.735-1.269 0.585-2.893-0.447-4.018z"
                                              fill="#f7b82b"/>
                                    </g>
                                </svg>
                            </div>
                            <div class="adv-name">
                                @lang('alert.exper')
                            </div>
                            <div class="adv-desc">
                                                                                                {!! \App\Models\ExperienceDescription::where(['language_id'=>$language_id,'experience_id'=>'5'])->first()->note!!}

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3 advantage-container animated wow fadeInRight" data-wow-duration="1s"
                         data-wow-delay="0.3s">
                        <div class="advantage">
                            <div class="adv-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve" width="50px"
                                     height="50px" class="animatedSVG">
<g>
    <g>
        <path d="M481.439,285.56c0-24.861-20.226-45.088-45.088-45.088H316.188c9.763-17.36,14.958-67.083,14.958-87.544    c0-34.599-28.149-62.748-62.748-62.748H256c-8.3,0-15.029,6.729-15.029,15.029c0,34.754-13.533,97.484-38.107,122.057    l-0.954,0.954c-16.98,16.98-31.247,23.088-51.114,33.022v-5.741c0-8.3-6.729-15.029-15.029-15.029H45.591    c-8.3,0-15.029,6.729-15.029,15.029v241.47c0,8.3,6.729,15.029,15.029,15.029h90.175c8.3,0,15.029-6.729,15.029-15.029v-9.207    C180.592,497.697,217.754,512,274.721,512h101.513c24.861,0,45.088-20.226,45.088-45.088c0-5.863-1.125-11.468-3.17-16.611    c19.126-5.22,33.229-22.747,33.229-43.506c0-5.863-1.125-11.468-3.17-16.611c19.126-5.22,33.229-22.747,33.229-43.506    c0-11.377-4.155-22.246-11.74-30.805C476.988,307.861,481.439,297.219,481.439,285.56z M120.737,285.56v181.353v15.029H60.62    V270.531h60.117V285.56z M436.351,361.708c-11.332,0-80.913,0-90.175,0c-8.3,0-15.029,6.729-15.029,15.029    c0,8.3,6.729,15.029,15.029,15.029h60.117c8.287,0,15.029,6.742,15.029,15.029s-6.742,15.029-15.029,15.029    c-6.371,0-53.746,0-60.117,0c-8.3,0-15.029,6.729-15.029,15.029c0,8.3,6.729,15.029,15.029,15.029h30.058    c8.287,0,15.029,6.742,15.029,15.029s-6.742,15.029-15.029,15.029H274.72c-52.451,0-84.988-12.883-123.925-25.861V294.848    c33.602-16.801,50.281-23.291,73.323-46.327c26.667-26.667,42.773-91.139,46.215-128.226    c17.129,1.004,30.755,15.257,30.755,32.632c0,23.363-9.099,75.388-25.656,91.946c-4.298,4.298-5.584,10.763-3.258,16.379    c2.326,5.616,7.806,9.278,13.885,9.278H436.35c8.287,0,15.029,6.742,15.029,15.029c0,8.287-6.741,15.03-15.028,15.03h-90.175    c-8.3,0-15.029,6.729-15.029,15.029c0,8.3,6.729,15.029,15.029,15.029h90.175c8.006,0,15.029,7.492,15.029,16.031    C451.38,354.966,444.638,361.708,436.351,361.708z"
              fill="#f7b82b"/>
    </g>
</g>
                                    <g>
                                        <g>
                                            <path d="M254.413,38.37L239.384,8.311c-3.711-7.423-12.738-10.434-20.163-6.721c-7.424,3.712-10.433,12.74-6.721,20.164    l15.029,30.058c3.712,7.426,12.741,10.432,20.163,6.721C255.116,54.822,258.125,45.794,254.413,38.37z"
                                                  fill="#f7b82b"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M352.897,1.59c-7.419-3.711-16.451-0.703-20.163,6.721L317.704,38.37c-3.712,7.424-0.703,16.452,6.721,20.164    c7.425,3.713,16.452,0.701,20.163-6.721l15.029-30.058C363.33,14.33,360.321,5.303,352.897,1.59z"
                                                  fill="#f7b82b"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M406.293,90.179h-30.058c-8.3,0-15.029,6.729-15.029,15.029c0,8.3,6.729,15.029,15.029,15.029h30.058    c8.3,0,15.029-6.729,15.029-15.029C421.322,96.908,414.593,90.179,406.293,90.179z"
                                                  fill="#f7b82b"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M195.883,90.179h-30.058c-8.3,0-15.029,6.729-15.029,15.029c0,8.3,6.729,15.029,15.029,15.029h30.058    c8.3,0,15.029-6.729,15.029-15.029C210.912,96.908,204.183,90.179,195.883,90.179z"
                                                  fill="#f7b82b"/>
                                        </g>
                                    </g>
</svg>
                            </div>
                            <div class="adv-name">
@lang('alert.translatIdea')                            </div>
                            <div class="adv-desc">
                                                         {!! \App\Models\ExperienceDescription::where(['language_id'=>$language_id,'experience_id'=>'6'])->first()->note!!}


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Fifth Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Sixth Section +++++ ----- ***** -->
    <section class="section" id="sec6">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title backed-tilte">
                                    @if(\App::isLocale('ar'))

خدماتنا                        
                        @else
                        Services
                        
                        @endif
                    </h3>
                </div>
                <div class="col-xs-12">
                    <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="943.000000pt" height="243.000000pt"
                         viewBox="0 0 943.000000 243.000000" preserveAspectRatio="xMidYMid meet" id="theAnim" style=""
                         class="">
                        <g transform="translate(0.000000,243.000000) scale(0.100000,-0.100000)" fill="#F2F3F4"
                           stroke="none" style="fill:none;stroke:#F2F3F4;stroke-width:35;">
                            <path d="M7710 2339 c-818 -37 -1646 -276 -2582 -745 -183 -92 -604 -322 -868
-474 -758 -437 -878 -504 -1141 -635 -320 -161 -609 -269 -854 -321 -165 -35
-419 -44 -566 -19 -570 95 -1050 564 -1494 1460 -46 94 -106 219 -133 279 -53
120 -72 141 -72 78 0 -28 21 -85 71 -194 439 -955 937 -1504 1496 -1648 253
-65 547 -53 863 34 382 105 731 275 1632 795 479 275 654 373 945 525 806 420
1498 662 2194 765 683 102 1410 61 2072 -118 l108 -29 -3 26 c-3 26 -10 29
-128 59 -356 93 -696 144 -1080 163 -222 10 -218 10 -460 -1z" class="cRNLMvEh_0"></path>
                        </g>
                        <style data-made-with="vivus-instant">.cRNLMvEh_0 {
                                stroke-dasharray: 21678 21680;
                                stroke-dashoffset: 21679;
                            }

                            .start .cRNLMvEh_0 {
                                animation: cRNLMvEh_draw 3000ms ease-in-out 0ms forwards;
                            }

                            @keyframes cRNLMvEh_draw {
                                100% {
                                    stroke-dashoffset: 0;
                                }
                            }

                            @keyframes cRNLMvEh_fade {
                                0% {
                                    stroke-opacity: 1;
                                }
                                94.44444444444444% {
                                    stroke-opacity: 1;
                                }
                                100% {
                                    stroke-opacity: 0;
                                }
                            }</style>
                    </svg>
                    <div class="col-xs-3 on-service-wrapper animated wow fadeInUp" data-wow-duration="0.5s"
                         data-wow-delay="3.25s">
                        <div class="dot"></div>
                        <div class="service-link">
                            <a href="#">@lang('alert.webDev')</a>
                        </div>
                        <div class="service-image-container animated">
                            <a href="{{route('Projects')}}">
                                <img src="{{asset('web/img/www.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 on-service-wrapper animated wow fadeInUp" data-wow-duration="0.5s"
                         data-wow-delay="3.5s">
                        <div class="dot"></div>
                        <div class="service-link">
                            <a href="#">@lang('alert.AppDev') </a>
                        </div>
                        <div class="service-image-container animated">
                            <a href="{{route('Projects')}}">
                                <img src="{{asset('web/img/mob.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 on-service-wrapper animated wow fadeInUp" data-wow-duration="0.5s"
                         data-wow-delay="3.75s">
                        <div class="dot"></div>
                        <div class="service-link">
                            <a href="#">@lang('alert.NetWorkTest')</a>
                        </div>
                        <div class="service-image-container animated">
                            <a href="{{route('Projects')}}">
                                <img src="{{asset('web/img/networking.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3 on-service-wrapper animated wow fadeInUp" data-wow-duration="0.5s"
                         data-wow-delay="3.9s">
                        <div class="dot"></div>
                        <div class="service-link">
                            <a href="#">@lang('alert.IOT')</a>
                        </div>
                        <div class="service-image-container animated">
                            <a href="{{route('Projects')}}">
                                <img src="{{asset('web/img/marketing.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Sixth Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Customers Section +++++ ----- ***** -->
    <section class="section" id="sec8">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title">
                        @lang('alert.CustomersOP')
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="testimonial-slider" class="owl-carousel">
                        
                        @foreach(App\Models\Feedback::all() as $feed)
                        <div class="testimonial">
                            <div class="commas">
                                <i class="ion-quote"></i>
                            </div>
                            <div class="pic">
                                <img src="{{asset('images/feedback/'.$feed->image)}}" alt="">
                            </div>
                            <div class="testimonial-review">
                                <h4 class="testimonial-title">
                                    {!! App\Models\FeedbackDescription::where(['feedback_id'=>$feed->id,'language_id'=>$language_id])->value('title') !!}
                                </h4>
                                <p>
                                    {!! App\Models\FeedbackDescription::where(['feedback_id'=>$feed->id,'language_id'=>$language_id])->value('note') !!}
                                </p>
                            </div>
                        </div>
    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Customers Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin ًWorks Section +++++ ----- ***** -->
    <section class="section" id="sec7">
        <div class="container">
            <div class="row">
                <h3 class="the-title backed-tilte" style="width:500 px;">
                    @if(\App::isLocale('ar'))
                        أعمالنا
@else
                        Projects
                    @endif
                </h3>
                <div class="works-slider owl-carousel owl-theme col-xs-12">
               @foreach($products as $product)
                   <div class="item col-xs-12">
                        <div class="col-sm-8 work-text-container hidden-xs">
                            <div class="work-text">
                                <h4 class="work-name">
                                    <a href="{{route('Project',$product->id)}}" class="hvr-ripple-out">{!! \App\Models\ProductDescription::where('product_id',$product->id)->first()->title!!}</a>
                                </h4>
                               {!! \App\Models\ProductDescription::where(['language_id'=>$language_id,'product_id'=>$product->id])->first()->note !!}
                            </div>
                        </div>
                        <div class="col-sm-4 work-image-container">
                            <div class="work-image">
                                <?php
                                    $image=\App\Models\ProductImage::where('product_id',$product->id)->value('name');
                                ?>
                                <img src="{{asset('images/product/'.$image)}}"/>
                                <div class="overlay-container">
                                    <div class="overlayed-text">
                                        <h4>{!! \App\Models\ProductDescription::where('product_id',$product->id)->first()->title!!}</h4>
                                        <div class="inner-proj-description">
                                            {!! \App\Models\ProductDescription::where(['language_id'=>$language_id,'product_id'=>$product->id])->first()->note !!}
                                        </div>
                                        <a href="{{route('Projects')}}" class="hvr-sweep-to-left go-to-proj">
                                        </a>
                                    </div>
                                </div>
                                <a class="iphone-btn hvr-radial-in"> </a>
                            </div>
                        </div>
                    </div>
                    <!--</div>-->
                @endforeach

                </div>
            </div>

        </div>
        <div class="container">
            <div class="row allProjects-container">
                <div class="col-xs-12">
                    <a href="{{route('Projects')}}" class="hvr-sweep-to-left allProjects">
                        @lang('alert.MoreProjects')
                    </a>
                </div>

            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Works Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Partners Section +++++ ----- ***** -->
    <section class="section" id="sec9">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-title">
                        @lang('alert.Partners')
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="partners-slider owl-carousel owl-theme col-xs-12">
                    @foreach($parts as $part)
                    <div class="item">
                        <img src="{{asset('images/partener/'.$part->image)}}"/>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Partners Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin Footer Section +++++ ----- ***** -->
@endsection

@section('isvisible')
<script>
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($("section#sec6").isVisible()) {
                $("#theAnim").addClass('start');
            }
        });
    });
</script>
@endsection