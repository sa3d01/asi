@extends('web.layouts.app')

@section('content')
    @if (session()->has('language'))
        @if (session()->get('language') == 'en')
            <?php $language_id=2;?>
        @else
            <?php $language_id=1;?>
        @endif
    @else
        <?php $language_id=1;?>
    @endif
<section id="allNewsSection" class="getME">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 styled-imagebg-container">
                    <div class="styled-imagebg">
                        <img src="{{asset('images/article/'.$new->image)}}" title="{{App\Models\ArticleDescription::where(['language_id'=>$language_id,'article_id'=>$new->id])->first()->title}}" alt="{{App\Models\ArticleDescription::where(['language_id'=>$language_id,'article_id'=>$new->id])->first()->title}}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="the-news-title wow animated fadeInDown" data-wow-duration="1s">
{{App\Models\ArticleDescription::where(['language_id'=>$language_id,'article_id'=>$new->id])->first()->title}}                    </h3>
                    <div class="the-news-text">
                        {!!App\Models\ArticleDescription::where(['language_id'=>$language_id,'article_id'=>$new->id])->first()->note!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 back-to-all-news">
                    <a href="{{route('NEWS')}}" class="hvr-sweep-to-left wow animated fadeInUp" data-wow-duration="1s">
                    {{trans('alert.Back')}}

                            <i class="ion-ios-arrow-thin-left"></i>
															</a>
                </div>
            </div>
        </div>
    </section>
    <!--        ***** ----- +++++ End Project Section +++++ ----- ***** -->
    <!--        ***** ----- +++++ Begin TOP BTN Section +++++ ----- ***** -->
    <a href="#" id="back-to-top">
												<i class="ion-android-arrow-up"></i>
											</a>

@stop


@section('scripts')

<script src="{{asset('web/js/typed.js')}}"></script>
    <script>
        ityped.init("#itypedOne" , {
            strings: [' لدينا فريق متحصص لمتابعة أحدث أخبار التقنية ...', 'لذا قم بمتابعتنا أينما كنت وتابع أحدث الأخبار  ...']
        })
    </script>
@stop