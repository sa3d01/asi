<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementDescription extends Model
{
    protected $fillable = ['note','title'];
}
