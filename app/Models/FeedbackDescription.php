<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackDescription extends Model
{
    protected $table='feedback_descriptions';
}
