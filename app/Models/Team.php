<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'image'];
    protected $table = 'teams';
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'add_by', 'id');
    }
    public function setImageAttribute($image)
    {
        $file = $image;
        $destinationPath = 'images/team/';
        $filename = $file->getClientOriginalName();
        $file->move($destinationPath, $filename);
        $this->attributes['image'] = $filename;
    }
}
