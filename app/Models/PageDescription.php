<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageDescription extends Model
{
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
