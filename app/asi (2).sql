-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 04, 2018 at 02:38 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asi`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `manager` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `date`, `manager`, `created_at`, `updated_at`) VALUES
(1, '2018-05-30', 'محمد أحمد', '2018-05-26 00:15:02', '2018-05-29 19:40:40'),
(2, '2018-05-16', 'ابراهيم المولى', '2018-05-26 00:16:35', '2018-05-29 19:39:56'),
(3, '2018-05-31', 'سعد', '2018-05-29 19:38:02', '2018-05-29 19:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `achievement_descriptions`
--

CREATE TABLE `achievement_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `achievement_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `manager` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `achievement_descriptions`
--

INSERT INTO `achievement_descriptions` (`id`, `achievement_id`, `language_id`, `manager`, `note`, `title`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'محمد أحمد', 'انجاز كبير', 'انجاز كبير', '2018-05-26 00:15:02', '2018-05-29 19:40:40'),
(2, 1, 2, 'mo', 'big achievement', 'achievement', '2018-05-26 00:15:02', '2018-05-29 19:40:40'),
(3, 2, 1, 'ابراهيم المولى', 'انجاز كبير اخر', 'انجاز اخر', '2018-05-26 00:16:35', '2018-05-29 19:39:56'),
(4, 2, 2, 'ibrahim', 'achievement', 'achievement', '2018-05-26 00:16:35', '2018-05-29 19:39:56'),
(5, 3, 1, 'سعد', 'انجاز تالى', 'انجاز', '2018-05-29 19:38:02', '2018-05-29 19:38:02'),
(6, 3, 2, 'saad', 'engaz', 'engaz', '2018-05-29 19:38:02', '2018-05-29 19:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `mobile`, `status`, `image`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '01092291228', 'active', NULL, '$2y$10$uhWf8eCjs4S6FMKl/k3YR.bemZIRJYZDGvc0zbmRPsopQa8QBri2W', 'Z6cruIQduH4Rg1ch3eFf0R47ekDfIPI9Y5hXcAGDuX2IkIeKJvB29cr2GCU5', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `status`, `add_by`, `image`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, '3.png', '2018-05-26 00:45:50', '2018-05-28 09:21:13'),
(2, 'active', 1, 'apple_logo_7-wallpaper-1680x1050.jpg', '2018-05-28 09:13:37', '2018-05-29 23:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `article_descriptions`
--

CREATE TABLE `article_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_descriptions`
--

INSERT INTO `article_descriptions` (`id`, `article_id`, `language_id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'الخبر الاول', 'مع اختلاف الألسنة تختلف القراءات التى تقرأ بها آيات الله حول العالم، حيث تعد قراءة "حفص عن عاصم" هى القراءة المتداولة فى مصر، ومع ذلك تقوم المطابع المصرية بطباعة المصاحف بالقراءات المختلفة على رأسها قراءة "ورش" المتعارف عليها فى دول أفريقيا الوسطى والمغرب العربى،  وقراءة "قالون" المتداولة فى عدد من الدول بينها ليبيا، وإرسالها إلى تلك الدول، فى الوقت الذى يراجع فيه المصحف الواحد 5 مرات قبل وصوله ليد القارىء، للتأكد من خلوه من أى أخطاء إملائية أو أخطاء بترتيب الصفحات والآيات', '2018-05-26 00:45:50', '2018-05-29 23:55:45'),
(2, 1, 2, 'first new', 'مع اختلاف الألسنة تختلف القراءات التى تقرأ بها آيات الله حول العالم، حيث تعد قراءة "حفص عن عاصم" هى القراءة المتداولة فى مصر، ومع ذلك تقوم المطابع المصرية بطباعة المصاحف بالقراءات المختلفة على رأسها قراءة "ورش" المتعارف عليها فى دول أفريقيا الوسطى والمغرب العربى،  وقراءة "قالون" المتداولة فى عدد من الدول بينها ليبيا، وإرسالها إلى تلك الدول، فى الوقت الذى يراجع فيه المصحف الواحد 5 مرات قبل وصوله ليد القارىء، للتأكد من خلوه من أى أخطاء إملائية أو أخطاء بترتيب الصفحات والآيات', '2018-05-26 00:45:50', '2018-05-29 23:55:45'),
(3, 2, 1, 'اخر الاخبار', '<p>ghxxh<br></p>', '2018-05-28 09:13:37', '2018-05-28 09:13:37'),
(4, 2, 2, 'last news', '<p>axgxjgx<br></p>', '2018-05-28 09:13:37', '2018-05-28 09:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `status` enum('active','not_active') DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `status`, `image`, `email`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 'active', 'fdept1.jpg', 'asi@asi.com', '01092291228', '2018-05-28 22:23:07', '2018-05-28 20:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `branche_descriptions`
--

CREATE TABLE `branche_descriptions` (
  `id` int(11) NOT NULL,
  `branche_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branche_descriptions`
--

INSERT INTO `branche_descriptions` (`id`, `branche_id`, `language_id`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'السودان', '2018-05-28 19:51:08', '2018-05-28 19:51:08'),
(2, 1, 2, 'sodan', '2018-05-28 19:51:08', '2018-05-28 19:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(1, 'محمد المرسى', 'promursi@gmail.com', '+966512345677', 'test text', '2018-05-29 13:55:18', '2018-05-29 13:55:18');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `status`, `add_by`, `created_at`, `updated_at`) VALUES
(3, 'active', 1, '2018-05-26 01:19:22', '2018-05-26 01:19:22'),
(4, 'active', 1, '2018-05-26 12:15:20', '2018-05-26 12:15:20'),
(5, 'active', 1, '2018-05-26 12:16:03', '2018-05-26 12:16:03'),
(6, 'active', 1, '2018-05-26 12:16:51', '2018-05-26 12:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `experience_descriptions`
--

CREATE TABLE `experience_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `experience_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experience_descriptions`
--

INSERT INTO `experience_descriptions` (`id`, `experience_id`, `language_id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(3, 3, 1, 'التطوير', 'التطويررر', '2018-05-26 01:19:22', '2018-05-30 00:00:01'),
(4, 3, 2, 'التطوير', 'التطوير', '2018-05-26 01:19:22', '2018-05-30 00:00:01'),
(5, 4, 1, 'تطبيقات الموبايل', 'تطبيقات الموبايل', '2018-05-26 12:15:20', '2018-05-30 00:01:14'),
(6, 4, 2, 'mobile apps', 'mobile apps', '2018-05-26 12:15:20', '2018-05-30 00:01:14'),
(7, 5, 1, 'خبرتنا', 'خبرتنا', '2018-05-26 12:16:03', '2018-05-30 00:01:45'),
(8, 5, 2, 'experiwnce', 'experiwnce', '2018-05-26 12:16:03', '2018-05-30 00:01:45'),
(9, 6, 1, 'ترجمة أفكاركم', 'ترجمة أفكاركم', '2018-05-26 12:16:51', '2018-05-30 00:00:36'),
(10, 6, 2, 'ideas translate', 'ideas translate', '2018-05-26 12:16:51', '2018-05-30 00:00:36');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `status`, `add_by`, `image`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, 'bread.jpg', '2018-05-26 01:33:36', '2018-05-29 23:47:11'),
(2, 'active', 1, '27858745_1997454383615324_6534568422172396500_n.jpg', '2018-05-29 23:46:02', '2018-05-29 23:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_descriptions`
--

CREATE TABLE `feedback_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `feedback_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback_descriptions`
--

INSERT INTO `feedback_descriptions` (`id`, `feedback_id`, `language_id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'احمد', 'شركه ممتازه', '2018-05-26 01:33:36', '2018-05-29 19:53:09'),
(2, 1, 2, 'ahmed', 'good company', '2018-05-26 01:33:36', '2018-05-29 23:47:02'),
(3, 2, 1, 'محمد السيد', 'جيده جدا الحقيقه', '2018-05-29 23:46:02', '2018-05-29 23:46:02'),
(4, 2, 2, 'mohamed elsaied', 'excielient company', '2018-05-29 23:46:02', '2018-05-29 23:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `hosts`
--

CREATE TABLE `hosts` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hosts`
--

INSERT INTO `hosts` (`id`, `status`, `add_by`, `price`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, '500', '2018-05-26 14:00:07', '2018-05-29 20:04:34'),
(2, 'active', 1, '150', '2018-05-26 14:10:46', '2018-05-26 14:10:46'),
(3, 'active', 1, '100', '2018-05-26 14:11:45', '2018-05-26 14:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `host_descriptions`
--

CREATE TABLE `host_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `host_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `host_descriptions`
--

INSERT INTO `host_descriptions` (`id`, `host_id`, `language_id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'الباقة الذهبية', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">200 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">4 دومين مجانى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:00:07', '2018-05-26 14:00:07'),
(2, 1, 2, 'golden', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">200 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">4 دومين مجانى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:00:07', '2018-05-26 14:00:07'),
(3, 2, 1, 'الباقة البرونزية', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">500 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">عدد لا نهائى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:10:46', '2018-05-26 14:10:46'),
(4, 2, 2, 'bronze package', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">500 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">عدد لا نهائى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:10:46', '2018-05-26 14:10:46'),
(5, 3, 1, 'الباقة الفضية', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">5464600 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">عدد لا نهائى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:11:45', '2018-05-28 08:28:20'),
(6, 3, 2, 'silver package', '<div class="baqa-storage" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">500 جيجا بايت</div><div class="baqa-domains" style="margin-bottom: 5px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">عدد لا نهائى</div><div class="baqa-support" style="margin-bottom: 20px; color: rgb(160, 162, 174); font-family: Cairo, sans-serif; text-align: center;">دعم فنى</div>', '2018-05-26 14:11:45', '2018-05-26 14:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `jops`
--

CREATE TABLE `jops` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jops`
--

INSERT INTO `jops` (`id`, `status`, `add_by`, `image`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, '3.png', '2018-05-26 20:32:45', '2018-05-26 20:45:07'),
(2, 'active', 1, '3.png', '2018-05-26 20:43:40', '2018-05-26 20:43:40'),
(3, 'active', 1, 'avatar.png', '2018-05-28 08:45:32', '2018-05-28 08:45:32'),
(4, 'active', 1, 'apple_logo_7-wallpaper-1680x1050.jpg', '2018-05-28 09:08:59', '2018-05-29 20:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `jop_descriptions`
--

CREATE TABLE `jop_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `jop_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `need` longtext CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jop_descriptions`
--

INSERT INTO `jop_descriptions` (`id`, `jop_id`, `language_id`, `title`, `note`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'مصمم جرافيك', 'نص اخر', 'متطلبات', '2018-05-26 20:32:45', '2018-05-29 20:24:22'),
(2, 1, 2, 'grafic designer', 'Virtual textp', 'Virtual textp', '2018-05-26 20:32:45', '2018-05-29 20:24:22'),
(3, 2, 1, 'اندرويد', 'نص', 'نص', '2018-05-26 20:43:40', '2018-05-29 20:23:59'),
(4, 2, 2, 'android', 'Virtual textp', 'Virtual textp', '2018-05-26 20:43:40', '2018-05-29 20:23:59'),
(5, 3, 1, 'مطور أندرويد', 'نص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعد', 'نص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعد', '2018-05-28 08:45:32', '2018-05-29 20:21:56'),
(6, 3, 2, 'Android Developer', 'Virtual textp', 'Virtual textp', '2018-05-28 08:45:32', '2018-05-29 20:21:57'),
(7, 4, 1, 'مبرمج اندرويد', 'نص افتراضى', 'نص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعدنص افتراضى يمكن أن يستبدل فيما بعد', '2018-05-28 09:08:59', '2018-05-29 20:20:49'),
(8, 4, 2, 'Android developer', 'text', 'text', '2018-05-28 09:08:59', '2018-05-29 20:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `label`, `status`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ar', 'active', 'اللغة العربية', NULL, NULL),
(2, 'en', 'active', 'English', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_02_14_102648_create_admins_table', 1),
(3, '2018_02_14_102715_create_clients_table', 1),
(4, '2018_02_15_101910_create_languages_table', 1),
(5, '2018_02_15_102948_create_settings_table', 1),
(6, '2018_02_15_142746_create_pages_table', 1),
(7, '2018_02_15_142759_create_page_descriptions_table', 1),
(8, '2018_02_16_152038_create_setting_descriptions_table', 1),
(9, '2018_02_16_152541_create_socials_table', 1),
(10, '2018_04_17_143149_create_users_table', 1),
(11, '2018_05_21_175500_create_contacts_table', 1),
(12, '2018_05_22_203643_create_teams_table', 1),
(13, '2018_05_22_205323_create_achievements_table', 1),
(14, '2018_05_25_011031_create_team_descriptions_table', 1),
(15, '2018_05_25_011339_create_achievement_descriptions_table', 1),
(16, '2018_05_25_012531_create_products_table', 1),
(17, '2018_05_25_012544_create_product_descriptions_table', 1),
(18, '2018_05_25_012618_create_parteners_table', 1),
(19, '2018_05_25_012646_create_feedbacks_table', 1),
(20, '2018_05_25_012655_create_feedback_descriptions_table', 1),
(21, '2018_05_25_012831_create_experiences_table', 1),
(22, '2018_05_25_012841_create_experience_descriptions_table', 1),
(23, '2018_05_25_013016_create_hosts_table', 1),
(24, '2018_05_25_013024_create_host_descriptions_table', 1),
(25, '2018_05_25_013302_create_articles_table', 1),
(26, '2018_05_25_013311_create_article_descriptions_table', 1),
(27, '2018_05_25_013400_create_jops_table', 1),
(28, '2018_05_25_013408_create_jop_descriptions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `logo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_descriptions`
--

CREATE TABLE `page_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parteners`
--

CREATE TABLE `parteners` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parteners`
--

INSERT INTO `parteners` (`id`, `status`, `add_by`, `image`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, 'bread.jpg', '2018-05-26 20:58:12', '2018-05-26 21:02:29'),
(2, 'active', 1, 'apple_style-wallpaper-1280x768.jpg', '2018-05-28 09:09:58', '2018-05-28 09:09:58');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('mobile','networking','marketing','web') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `android` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `status`, `type`, `add_by`, `android`, `ios`, `video`, `created_at`, `updated_at`) VALUES
(5, 'not_active', 'web', 1, NULL, NULL, NULL, '2018-05-26 22:56:59', '2018-05-30 00:05:49'),
(6, 'active', 'mobile', 1, 'https://www.facebook.com/', NULL, NULL, '2018-05-26 22:57:18', '2018-05-26 23:07:12'),
(7, 'active', 'web', 1, NULL, NULL, NULL, '2018-05-29 08:25:38', '2018-05-29 08:25:38'),
(8, 'not_active', 'mobile', 1, NULL, NULL, NULL, '2018-05-29 08:26:44', '2018-05-30 00:05:44'),
(9, 'active', 'mobile', 1, NULL, NULL, 'blank.mp4', '2018-05-30 00:20:37', '2018-05-30 00:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_descriptions`
--

CREATE TABLE `product_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_descriptions`
--

INSERT INTO `product_descriptions` (`id`, `product_id`, `language_id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(9, 5, 1, 'تطبيق CRP', 'تطبيق جيد جدا', '2018-05-26 22:56:59', '2018-05-27 18:55:00'),
(10, 5, 2, 'تطبيق CRP', 'تطبيق ممتاز', '2018-05-26 22:56:59', '2018-05-27 18:55:00'),
(11, 6, 1, 'تطبيق CRP', 'تطبيق رائع ملئ بالمميزات', '2018-05-26 22:57:18', '2018-05-27 18:53:44'),
(12, 6, 2, 'تطبيق CRP', 'good product', '2018-05-26 22:57:18', '2018-05-27 18:53:44'),
(13, 7, 1, 'موقع الزبون', 'موقع', '2018-05-29 08:25:38', '2018-05-30 00:05:36'),
(14, 7, 2, 'zboon', 'web', '2018-05-29 08:25:38', '2018-05-29 08:25:38'),
(15, 8, 1, 'موقع الزبون', 'موقع', '2018-05-29 08:26:44', '2018-05-30 00:05:19'),
(16, 8, 2, 'zboon', 'website', '2018-05-29 08:26:44', '2018-05-30 00:05:19'),
(17, 9, 1, 'vvvvvvvvvv', 'vvvvvvvvvvvvvv', '2018-05-30 00:20:37', '2018-05-30 00:20:37'),
(18, 9, 2, 'vvvvvvvvvvvvv', 'vvvvvvvvvvvv', '2018-05-30 00:20:37', '2018-05-30 00:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `add_by` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `add_by`, `product_id`, `name`, `created_at`, `updated_at`) VALUES
(10, NULL, 6, '2.png', '2018-05-27 18:53:45', '2018-05-27 18:53:45'),
(11, NULL, 5, 'bread.jpg', '2018-05-27 18:55:00', '2018-05-27 18:55:00'),
(9, NULL, 6, '3.png', '2018-05-27 18:53:45', '2018-05-27 18:53:45'),
(8, NULL, 6, 'bread.jpg', '2018-05-27 18:53:44', '2018-05-27 18:53:44'),
(12, NULL, 5, '3.png', '2018-05-27 18:55:00', '2018-05-27 18:55:00'),
(13, NULL, 5, '2.png', '2018-05-27 18:55:00', '2018-05-27 18:55:00'),
(14, 1, 7, 'fdept1.jpg', '2018-05-29 08:25:38', '2018-05-29 08:25:38'),
(15, 1, 8, '13254311_943457885771664_3903561653940907121_n.jpg', '2018-05-29 08:26:44', '2018-05-29 08:26:44'),
(16, 1, 8, '13233127_943457599105026_71343985246464390_n.jpg', '2018-05-29 08:26:44', '2018-05-29 08:26:44'),
(17, 1, 8, '13254489_943457289105057_8842087078399886543_n.jpg', '2018-05-29 08:26:44', '2018-05-29 08:26:44'),
(18, 1, 9, 'bread.jpg', '2018-05-30 00:20:37', '2018-05-30 00:20:37'),
(19, 1, 9, '3.png', '2018-05-30 00:20:37', '2018-05-30 00:20:37'),
(20, 1, 9, '2.png', '2018-05-30 00:20:37', '2018-05-30 00:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `mobile_1`, `mobile_2`, `email`, `lat`, `long`, `created_at`, `updated_at`) VALUES
(1, 'logo.png', '01092291228', '01122435541', 'admin@admin.com', '24.665482501303877', '46.76317704353028', '2018-05-21 22:00:00', '2018-05-28 09:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `setting_descriptions`
--

CREATE TABLE `setting_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_descriptions`
--

INSERT INTO `setting_descriptions` (`id`, `setting_id`, `language_id`, `name`, `address`, `about`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'asi', 'مصر , المنصورة\r\n', '<p>عن ASI</p>', NULL, '2018-05-25 23:22:19'),
(2, 1, 2, 'asi', 'مصر , المنصورة\r\n', 'about ASI', NULL, '2018-05-25 23:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_by` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `status`, `add_by`, `name`, `link`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, 'facebook', 'https://www.facebook.com/?sk=h_chr', '2018-05-26 23:29:45', '2018-05-26 23:29:45'),
(2, 'active', 1, 'twitter', 'https://twitter.com/', '2018-05-26 23:29:55', '2018-05-26 23:29:55'),
(3, 'active', 1, 'instgram', 'https://www.instagram.com/', '2018-05-26 23:31:58', '2018-05-26 23:31:58'),
(4, 'active', 1, 'linkedin', 'https://www.linkedin.com/feed/', '2018-05-28 09:10:13', '2018-05-28 09:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'محمد', '3.png', '2018-05-26 23:27:15', '2018-05-29 19:46:52'),
(2, 'عمر', 'apple_logo_7-wallpaper-1680x1050.jpg', '2018-05-28 09:33:31', '2018-05-29 19:47:06');

-- --------------------------------------------------------

--
-- Table structure for table `team_descriptions`
--

CREATE TABLE `team_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_descriptions`
--

INSERT INTO `team_descriptions` (`id`, `team_id`, `language_id`, `job`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'مصمم', 'محمد', '2018-05-26 23:27:15', '2018-05-29 19:46:52'),
(2, 1, 2, 'graphic', 'mohamed', '2018-05-26 23:27:15', '2018-05-29 19:46:52'),
(3, 2, 1, 'مبرمج اندرويد', 'عمر', '2018-05-28 09:33:31', '2018-05-29 19:47:07'),
(4, 2, 2, 'Android developer', 'omar', '2018-05-28 09:33:31', '2018-05-29 19:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('union','user') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `achievement_descriptions`
--
ALTER TABLE `achievement_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `achievement_descriptions_achievement_id_foreign` (`achievement_id`),
  ADD KEY `achievement_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_mobile_unique` (`mobile`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_add_by_foreign` (`add_by`);

--
-- Indexes for table `article_descriptions`
--
ALTER TABLE `article_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_descriptions_article_id_foreign` (`article_id`),
  ADD KEY `article_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branche_descriptions`
--
ALTER TABLE `branche_descriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_username_unique` (`username`),
  ADD UNIQUE KEY `clients_email_unique` (`email`),
  ADD UNIQUE KEY `clients_mobile_unique` (`mobile`),
  ADD KEY `clients_add_by_foreign` (`add_by`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `experiences_add_by_foreign` (`add_by`);

--
-- Indexes for table `experience_descriptions`
--
ALTER TABLE `experience_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `experience_descriptions_experience_id_foreign` (`experience_id`),
  ADD KEY `experience_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedbacks_add_by_foreign` (`add_by`);

--
-- Indexes for table `feedback_descriptions`
--
ALTER TABLE `feedback_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedback_descriptions_feedback_id_foreign` (`feedback_id`),
  ADD KEY `feedback_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `hosts`
--
ALTER TABLE `hosts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hosts_add_by_foreign` (`add_by`);

--
-- Indexes for table `host_descriptions`
--
ALTER TABLE `host_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_descriptions_host_id_foreign` (`host_id`),
  ADD KEY `host_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `jops`
--
ALTER TABLE `jops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jops_add_by_foreign` (`add_by`);

--
-- Indexes for table `jop_descriptions`
--
ALTER TABLE `jop_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jop_descriptions_jop_id_foreign` (`jop_id`),
  ADD KEY `jop_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_add_by_foreign` (`add_by`);

--
-- Indexes for table `page_descriptions`
--
ALTER TABLE `page_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_descriptions_page_id_foreign` (`page_id`),
  ADD KEY `page_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `parteners`
--
ALTER TABLE `parteners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parteners_add_by_foreign` (`add_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_add_by_foreign` (`add_by`);

--
-- Indexes for table `product_descriptions`
--
ALTER TABLE `product_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_descriptions_product_id_foreign` (`product_id`),
  ADD KEY `product_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_descriptions`
--
ALTER TABLE `setting_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_descriptions_setting_id_foreign` (`setting_id`),
  ADD KEY `setting_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `socials_add_by_foreign` (`add_by`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_descriptions`
--
ALTER TABLE `team_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_descriptions_team_id_foreign` (`team_id`),
  ADD KEY `team_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `achievement_descriptions`
--
ALTER TABLE `achievement_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `article_descriptions`
--
ALTER TABLE `article_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `branche_descriptions`
--
ALTER TABLE `branche_descriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `experience_descriptions`
--
ALTER TABLE `experience_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `feedback_descriptions`
--
ALTER TABLE `feedback_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hosts`
--
ALTER TABLE `hosts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `host_descriptions`
--
ALTER TABLE `host_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jops`
--
ALTER TABLE `jops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jop_descriptions`
--
ALTER TABLE `jop_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_descriptions`
--
ALTER TABLE `page_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parteners`
--
ALTER TABLE `parteners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `product_descriptions`
--
ALTER TABLE `product_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting_descriptions`
--
ALTER TABLE `setting_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `team_descriptions`
--
ALTER TABLE `team_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `achievement_descriptions`
--
ALTER TABLE `achievement_descriptions`
  ADD CONSTRAINT `achievement_descriptions_achievement_id_foreign` FOREIGN KEY (`achievement_id`) REFERENCES `achievements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `achievement_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `article_descriptions`
--
ALTER TABLE `article_descriptions`
  ADD CONSTRAINT `article_descriptions_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `experiences`
--
ALTER TABLE `experiences`
  ADD CONSTRAINT `experiences_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `experience_descriptions`
--
ALTER TABLE `experience_descriptions`
  ADD CONSTRAINT `experience_descriptions_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experiences` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `experience_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD CONSTRAINT `feedbacks_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `feedback_descriptions`
--
ALTER TABLE `feedback_descriptions`
  ADD CONSTRAINT `feedback_descriptions_feedback_id_foreign` FOREIGN KEY (`feedback_id`) REFERENCES `feedbacks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `feedback_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hosts`
--
ALTER TABLE `hosts`
  ADD CONSTRAINT `hosts_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `host_descriptions`
--
ALTER TABLE `host_descriptions`
  ADD CONSTRAINT `host_descriptions_host_id_foreign` FOREIGN KEY (`host_id`) REFERENCES `hosts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `host_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jops`
--
ALTER TABLE `jops`
  ADD CONSTRAINT `jops_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jop_descriptions`
--
ALTER TABLE `jop_descriptions`
  ADD CONSTRAINT `jop_descriptions_jop_id_foreign` FOREIGN KEY (`jop_id`) REFERENCES `jops` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jop_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_descriptions`
--
ALTER TABLE `page_descriptions`
  ADD CONSTRAINT `page_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `page_descriptions_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parteners`
--
ALTER TABLE `parteners`
  ADD CONSTRAINT `parteners_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_descriptions`
--
ALTER TABLE `product_descriptions`
  ADD CONSTRAINT `product_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_descriptions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `setting_descriptions`
--
ALTER TABLE `setting_descriptions`
  ADD CONSTRAINT `setting_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `setting_descriptions_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `socials`
--
ALTER TABLE `socials`
  ADD CONSTRAINT `socials_add_by_foreign` FOREIGN KEY (`add_by`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `team_descriptions`
--
ALTER TABLE `team_descriptions`
  ADD CONSTRAINT `team_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `team_descriptions_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
