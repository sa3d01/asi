<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use App\Models\DistrictDescription;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use App\Models\CityDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function signup($lang, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'unique:users',
            'mobile' => 'unique:users',
            'email' => 'unique:users',
        ]);
        if ($validator->passes()) {
            $user = new User;
            $user->username = $request->username;
            $user->mobile = $request->mobile;
            if($request->email){
                $user->email = $request->email;
            }
            $user->status = 'active';
            $user->type = 'user';
            $user->password = request('password');
            $user->save();
            $arr['id'] = (string)$user->id;
            $arr['username'] = (string)$user->username;
            $arr['mobile'] = (string)$user->mobile;
            $arr['type'] = $user->type;
            $arr['name'] = "";
            if($user->email){
                $arr['emil']= $user->email;
            }else{
                $arr['emil'] = "";
            }

            return response()->json(['value' => true, 'data' => $arr]);
        } else {
            foreach ((array)$validator->errors() as $value) {

                if (isset($value['username'])) {
                    $lang == 'ar' ? $msg = 'اسم المستخدم موجود بالفعل ' : $msg = 'Invalid username';
                    return response()->json([
                        'error' => 2,
                        'value' => false,
                        'msg' => $msg
                    ]);
                } elseif (isset($value['mobile'])) {
                    $lang == 'ar' ? $msg = 'رقم الجوال موجود بالفعل' : $msg = 'Invalid mobile number';
                    return response()->json([
                        'error' => 3,
                        'value' => false,
                        'msg' => $msg
                    ]);
                }elseif (isset($value['email'])) {
                    $lang == 'ar' ? $msg = 'البريد موجود بالفعل' : $msg = 'Invalid mobile number';
                    return response()->json([
                        'error' => 4,
                        'value' => false,
                        'msg' => $msg
                    ]);
                }else {
                    $lang == 'ar' ? $msg = 'لم يتم التسجيل' : $msg = 'No registration';
                    return response()->json([
                        'error' => 0,
                        'value' => false,
                        'msg' => $msg
                    ]);
                }
            }
        }


    }

    public function signin($lang, Request $request)
    {
        $validate = Validator::make($request->all(), [
            'type' => 'required'
        ]);

        if ($validate->fails()) {
            foreach ((array)$validate->errors() as $value) {
                if (isset($value['type'])) {
                    $lang == 'ar' ? $msg = 'نوع العضو مطلوب' : $msg = 'Member Type Required';
                    return response()->json([
                        'value' => false,
                        'error' => '5',
                        'msg' => $msg
                    ]);
                } else {
                    $lang == 'ar' ? $msg = 'لم يتم التسجيل' : $msg = 'No registration';
                    return response()->json([
                        'value' => false,
                        'error' => '0',
                        'msg' => $msg
                    ]);
                }
            }
        }
        if(strpos($request->user_name,'05')===false){
            $data=['username' => $request->input('user_name'),'password' => $request->input('password'),'status'=>'active'];
            $false_data=['username' => $request->input('user_name'),'password' => $request->input('password'),'status'=>'not_active'];
            $user = User::where(['username' => $request->user_name, 'type' => $request->type])->first();
            $client = Client::where(['username' => $request->user_name])->first();
        }else{
            $data=['mobile' => $request->input('user_name'),'password' => $request->input('password'),'status'=>'active'];
            $false_data=['mobile' => $request->input('user_name'),'password' => $request->input('password'),'status'=>'not_active'];
            $user = User::where(['mobile' => $request->user_name, 'type' => $request->type])->first();
            $client = Client::where(['mobile' => $request->user_name])->first();
        }
        if (!$user && !$client) {
            $lang == 'ar' ? $msg = 'هذا المستخدم غير مسجل' : $msg = 'This user is not registered';
            return response()->json([
                'value' => false,
                'error' => '6',
                'msg' => $msg
            ]);
        }

        $arr = [];
        if($request->type=='owner'){
            if (Auth::guard('client')->attempt($data)) {
                $arr['id'] = (string)$client->id;
                if($client->name){
                    $arr['name'] = $client->name;
                }else{
                    $arr['name'] = "";
                }
                $arr['username'] = $client->username;
                if($client->email){
                    $arr['email'] = $client->email;
                }else{
                    $arr['email'] = "";
                }
                if ($client->image == null) {
                    $arr['image'] = "";
                } else {
                    $arr['image'] = asset('images/user/' . $client->image);
                }
                $arr['mobile'] = (string)$client->mobile;
                $arr['latitude'] = (string)$client->latitude;
                $arr['longitude'] = (string)$client->longitude;
                $arr['type'] = 'owner';
                if($client->district_id){
                    $arr['district_id'] = (string)$client->district_id;
                    if ($lang == 'ar') {
                        $district_name = DistrictDescription::where(['district_id' => $client->district_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $district_name = DistrictDescription::where(['district_id' => $client->district_id, 'language_id' => 2])->value('name');
                    }
                    $arr['district_name'] = $district_name;
                    $arr['city_id'] = (string)$client->district->city_id;
                    if ($lang == 'ar') {
                        $city_name = CityDescription::where(['city_id' => $client->district->city_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $city_name = CityDescription::where(['city_id' => $client->district->city_id, 'language_id' => 2])->value('name');
                    }
                    $arr['city_name'] = $city_name;
                }else{
                    $arr['district_id'] = "";
                    $arr['district_name'] = "";
                    $arr['city_id'] = "";
                    $arr['city_name'] = "";
                }
                return response()->json(['value' => true, 'data' => $arr]);
            } elseif (Auth::guard('client')->attempt($data)) {
                $lang == 'ar' ? $msg = 'يرجى التواصل مع الادارة' : $msg = 'Please contact the administration';

                return response()->json([
                    'value' => false,
                    'msg' => $msg
                ]);
            } else {
                $lang == 'ar' ? $msg = 'كلمة المرور غير صحيحة' : $msg = 'The password is incorrect';
                return response()->json([
                    'value' => false,
                    'msg' => $msg
                ]);
            }
        }
        if (Auth::attempt($data)) {
            $user = Auth::user();
            $arr['id'] = (string)$user->id;
            if($user->name){
                $arr['name'] = $user->name;
            }else{
                $arr['name'] = "";
            }
            $arr['username'] = $user->username;
            if($user->email){
                $arr['email'] = $user->email;
            }else{
                $arr['email'] = "";
            }
            if ($user->image == null) {
                $arr['image'] = "";
            } else {
                $arr['image'] = asset('images/user/' . $user->image);
            }
            $arr['mobile'] = (string)$user->mobile;
            $arr['latitude'] = (string)$user->latitude;
            $arr['longitude'] = (string)$user->longitude;
            $arr['type'] = $user->type;
            if($user->district_id){
                $arr['district_id'] = (string)$user->district_id;
                if ($lang == 'ar') {
                    $district_name = DistrictDescription::where(['district_id' => $user->district_id, 'language_id' => 1])->value('name');
                } elseif ($lang == 'en') {
                    $district_name = DistrictDescription::where(['district_id' => $user->district_id, 'language_id' => 2])->value('name');
                }
                $arr['district_name'] = $district_name;
                $arr['city_id'] = (string)$user->district->city_id;
                if ($lang == 'ar') {
                    $city_name = CityDescription::where(['city_id' => $user->district->city_id, 'language_id' => 1])->value('name');
                } elseif ($lang == 'en') {
                    $city_name = CityDescription::where(['city_id' => $user->district->city_id, 'language_id' => 2])->value('name');
                }
                $arr['city_name'] = $city_name;
            }else{
                $arr['district_id'] = "";
                $arr['district_name'] = "";
                $arr['city_id'] = "";
                $arr['city_name'] = "";
            }
            return response()->json(['value' => true, 'data' => $arr]);
        } elseif (Auth::attempt($false_data)) {
            $lang == 'ar' ? $msg = 'يرجى التواصل مع الادارة' : $msg = 'Please contact the administration';
            return response()->json([
                'value' => false,
                'msg' => $msg
            ]);
        } else {
            $lang == 'ar' ? $msg = 'كلمة المرور غير صحيحة' : $msg = 'The password is incorrect';
            return response()->json([
                'value' => false,
                'msg' => $msg
            ]);
        }
    }

    public function update_profile($id,$lang, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'max:100|unique:users,username,'.$id,
            'mobile' => 'unique:users,mobile,'.$id,
        ]);
        if ($validator->passes()) {
            $update_profile = User::find($id);
            if ($request->username) {
                $update_profile->username = $request->username;
            }
            if ($request->mobile) {
                $update_profile->mobile = $request->mobile;
            }
            if ($request->email) {
                $update_profile->email = $request->email;
            }

            if ($request->password && $request->password !== ' ') {
                $update_profile->password = request('password');
            }

            $update_profile->update();

            $arr['id'] = (string)$update_profile->id;
            $arr['username'] = $update_profile->username;
            $arr['email'] = $update_profile->email;
            $arr['mobile'] = (string)$update_profile->mobile;
            $arr['type'] = $update_profile->type;
            $arr['name'] = "";
            return response()->json(['value' => 'true', 'data' => $arr]);

        } else {
            foreach ((array)$validator->errors() as $value) {
                if (isset($value['username'])) {
                    $lang == 'ar' ? $msg = 'اسم المستخدم مستخدم من قبل ' : $msg = 'Invalid username';
                    return response()->json([
                        'error' => 1,
                        'value' => false,
                        'msg' => $msg
                    ]);
                } elseif (isset($value['mobile'])) {
                    $lang == 'ar' ? $msg = 'رقم الجوال مستخدم من قبل ' : $msg = 'Invalid mobile number';
                    return response()->json([
                        'error' => 2,
                        'value' => false,
                        'msg' => $msg
                    ]);
                }else{
                    return response()->json([
                        'error' => 3,
                        'value' => false,
                    ]);
                }
            }
        }
    }

    public function confirmation_sms($lang,Request $request){
        $username='966551783558';
        $password='TATWEERS***2030';
        $sender='iee3lanweb';
        $dat=date("Y-m-d");
        $time=date("h:m");
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $random_code = '';
        $length = 4;
        for ($i = 0; $i < $length; $i++) {
            $random_code .= $characters[rand(0, $charactersLength - 1)];
        }
        $message=urlencode($random_code);
        $user=User::where('mobile',$request->input('mobile'))->first();
        $client=Client::where('mobile',$request->input('mobile'))->first();
        if(!$user && !$client){
            $lang == 'ar' ? $msg = 'هذا المستخدم غير مسجل' : $msg = 'This user is not registered';
            return response()->json(['value' => false,'msg'=>$msg]);
        }
        $numbers=urlencode($request->input('mobile'));
        $data='https://www.hisms.ws/api.php?send_sms&username='.$username."&password=".$password
            ."&numbers=".$numbers."&sender=".$sender."&message=".$message."&date=".$dat."&time=".$time;
        $clients = new \GuzzleHttp\Client();
        $res = $clients->get($data);
        if($res->getStatusCode()) {
            if($user){
                $user->activation_code=$message;
                $user->update();
                $arr['id'] = (string)$user->id;
                $arr['activation_code'] = $message;
                if($user->name){
                    $arr['name'] = $user->name;
                }else{
                    $arr['name'] = "";
                }
                $arr['username'] = $user->username;
                if($user->email){
                    $arr['email'] = $user->email;
                }else{
                    $arr['email'] = "";
                }
                if ($user->image == null) {
                    $arr['image'] = "";
                } else {
                    $arr['image'] = asset('images/user/' . $user->image);
                }
                $arr['mobile'] = (string)$user->mobile;
                $arr['latitude'] = (string)$user->latitude;
                $arr['longitude'] = (string)$user->longitude;
                $arr['type'] = $user->type;
                if($user->district_id){
                    $arr['district_id'] = (string)$user->district_id;
                    if ($lang == 'ar') {
                        $district_name = DistrictDescription::where(['district_id' => $user->district_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $district_name = DistrictDescription::where(['district_id' => $user->district_id, 'language_id' => 2])->value('name');
                    }
                    $arr['district_name'] = $district_name;
                    $arr['city_id'] = (string)$user->district->city_id;
                    if ($lang == 'ar') {
                        $city_name = CityDescription::where(['city_id' => $user->district->city_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $city_name = CityDescription::where(['city_id' => $user->district->city_id, 'language_id' => 2])->value('name');
                    }
                    $arr['city_name'] = $city_name;
                }else{
                    $arr['district_id'] = "";
                    $arr['district_name'] = "";
                    $arr['city_id'] = "";
                    $arr['city_name'] = "";
                }
            }
            elseif ($client){
                $client->activation_code=$message;
                $client->update();
                $arr['id'] = (string)$client->id;
                $arr['activation_code'] = $message;
                if($client->name){
                    $arr['name'] = $client->name;
                }else{
                    $arr['name'] = "";
                }
                $arr['username'] = $client->username;
                if($client->email){
                    $arr['email'] = $client->email;
                }else{
                    $arr['email'] = "";
                }
                if ($client->image == null) {
                    $arr['image'] = "";
                } else {
                    $arr['image'] = asset('images/user/' . $client->image);
                }
                $arr['mobile'] = (string)$client->mobile;
                $arr['latitude'] = (string)$client->latitude;
                $arr['longitude'] = (string)$client->longitude;
                $arr['type'] = 'owner';
                if($client->district_id){
                    $arr['district_id'] = (string)$client->district_id;
                    if ($lang == 'ar') {
                        $district_name = DistrictDescription::where(['district_id' => $client->district_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $district_name = DistrictDescription::where(['district_id' => $client->district_id, 'language_id' => 2])->value('name');
                    }
                    $arr['district_name'] = $district_name;
                    $arr['city_id'] = (string)$client->district->city_id;
                    if ($lang == 'ar') {
                        $city_name = CityDescription::where(['city_id' => $client->district->city_id, 'language_id' => 1])->value('name');
                    } elseif ($lang == 'en') {
                        $city_name = CityDescription::where(['city_id' => $client->district->city_id, 'language_id' => 2])->value('name');
                    }
                    $arr['city_name'] = $city_name;
                }else{
                    $arr['district_id'] = "";
                    $arr['district_name'] = "";
                    $arr['city_id'] = "";
                    $arr['city_name'] = "";
                }
            }
            return response()->json(['value' => true,'data'=>$arr]);
        }else{
            return response()->json(['value' => false]);
        }
    }
}
