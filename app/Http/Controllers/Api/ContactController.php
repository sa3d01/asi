<?php

namespace App\Http\Controllers\Api;

use App\Models\Bank;
use App\Models\BankDescription;
use App\Models\BuildingStatus;
use App\Models\BuildingStatusDescription;
use App\Models\BuildingType;
use App\Models\BuildingTypeDescription;
use App\Models\City;
use App\Models\CityDescription;
use App\Models\Contact;
use App\Models\District;
use App\Models\DistrictDescription;
use App\Models\Favourite;
use App\Models\MonthBill;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->mobile = $request->mobile;
        $contact->content = $request->input('content');
        $contact->save();
        return response()->json(['value'=> true]);
    }
    public function favourite(Request $request)
    {
        if($request->is_flat=='1'){
            $favourite=Favourite::where(['user_id'=>$request->user_id,'flat_id'=>$request->building_id])->first();
        }else{
            $favourite=Favourite::where(['user_id'=>$request->user_id,'building_id'=>$request->building_id])->first();
        }
        if($favourite){
            $favourite->delete();
        }else{
            $favourite = new Favourite();
            $favourite->user_id = $request->user_id;
            if($request->is_flat=='1'){
                $favourite->flat_id = $request->building_id;
            }else{
                $favourite->building_id = $request->building_id;
            }
            $favourite->save();
        }
        return response()->json(['value'=> true]);
    }
    public function banks($lang)
    {
        $banks = Bank::all();
        $data = [];
        foreach ($banks as $bank) {
            $arr['id'] = (string)$bank->id;
            $arr['number'] = (string)$bank->number;
            if (!$bank->logo) {
                $arr['image'] = (string)asset('images/logo/default.png');
            } else {
                $arr['image'] = (string)asset('images/bank/'.$bank->logo);
            }
            if ($lang == 'ar') {
                $arr['name'] = BankDescription::where(['language_id' => 1, 'bank_id' => $bank->id])->value('name');
            } else {
                $arr['name'] = BankDescription::where(['language_id' => 2, 'bank_id' => $bank->id])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function bill(Request $request)
    {
        if(!$request->user_id || !$request->month_id)
        {
            return response()->json(['value' => false]);
        }
//        $file = base64_decode($request->bill);
//        $safeName = str_random(10) . '.' . 'png';
//        file_put_contents(public_path() . '/images/bill/'.$safeName,$file);
        $file= $request->file('bill');
        $destinationPath = public_path() .'/images/bill/';
        $extension =  $file->clientExtension();
        $filename=str_random(10).'.'.$extension;
        $file->move($destinationPath, $filename);

        $month_bill=new MonthBill();
        $month_bill->image=$filename;
        $month_bill->month_id=$request->month_id;
        $month_bill->user_id=$request->user_id;
        $month_bill->confirm='0';
        $month_bill->save();
        return response()->json(['value' => true]);
    }
    public function building_status($lang)
    {
        $building_statuses = BuildingStatus::all();
        $data = [];
        foreach ($building_statuses as $building_status) {
            $arr['id'] = (string)$building_status->id;
            if ($lang == 'ar') {
                $arr['name'] = BuildingStatusDescription::where(['language_id' => 1, 'building_status_id' => $building_status->id])->value('name');
            } else {
                $arr['name'] = BuildingStatusDescription::where(['language_id' => 2, 'building_status_id' => $building_status->id])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function building_type($lang)
    {
        $building_types = BuildingType::all();
        $data = [];
        foreach ($building_types as $building_type) {
            $arr['id'] = (string)$building_type->id;
            if ($lang == 'ar') {
                $arr['name'] = BuildingTypeDescription::where(['language_id' => 1, 'building_type_id' => $building_type->id])->value('name');
            } else {
                $arr['name'] = BuildingTypeDescription::where(['language_id' => 2, 'building_type_id' => $building_type->id])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function city($lang)
    {
        $cities = City::all();
        $data = [];
        foreach ($cities as $city) {
            $arr['id'] = (string)$city->id;
            if ($lang == 'ar') {
                $arr['name'] = CityDescription::where(['language_id' => 1, 'city_id' => $city->id])->value('name');
            } else {
                $arr['name'] = CityDescription::where(['language_id' => 2, 'city_id' => $city->id])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function district($id,$lang)
    {
        $districts = District::where('city_id',$id)->get();
        $data = [];
        foreach ($districts as $district) {
            $arr['id'] = (string)$district->id;
            if ($lang == 'ar') {
                $arr['name'] = DistrictDescription::where(['language_id' => 1, 'district_id' => $district->id])->value('name');
            } else {
                $arr['name'] = DistrictDescription::where(['language_id' => 2, 'district_id' => $district->id])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }


}
