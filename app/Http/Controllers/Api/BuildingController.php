<?php

namespace App\Http\Controllers\Api;


use App\Models\Building;
use App\Models\BuildingDescription;
use App\Models\BuildingImage;
use App\Models\BuildingStatusDescription;
use App\Models\CityDescription;
use App\Models\City;
use App\Models\Client;
use App\Models\DistrictDescription;
use App\Models\Favourite;
use App\Models\Flat;
use App\Models\Hiring;
use App\Models\Month;
use App\Models\MonthBill;
use App\Models\MonthDescription;
use App\Models\SettingDescription;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BuildingController extends Controller
{
    public function buildings($id=0,$lang,Request $request)
    {
        $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0'])->get();
        if ($request->district_id && $request->building_type_id && $request->building_status_id) {
            $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0','district_id'=>$request->district_id,'building_status_id'=>$request->building_status_id,'building_type_id'=>$request->building_type_id])->get();
        } elseif ($request->building_status_id && $request->building_type_id) {
            $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0','building_status_id'=>$request->building_status_id,'building_type_id'=>$request->building_type_id])->get();
        } elseif ($request->building_type_id) {
            $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0','building_type_id'=>$request->building_type_id])->get();
        }elseif ($request->building_status_id) {
            $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0','building_status_id'=>$request->building_status_id])->get();
        }elseif ($request->district_id) {
            $buildings = Building::where(['status' => 'active', 'type' => 'owner','have_flats'=>'0','district_id'=>$request->district_id])->get();
        }
        $data = [];
        foreach ($buildings as $building) {
            $arr['id'] = (string)$building->id;
            $favourite=Favourite::where(['user_id'=>$id,'building_id'=>$building->id])->first();
            if($favourite){
                $arr['favourite'] = true;
            }else{
                $arr['favourite'] = false;
            }
            $arr['is_flat'] = '0';
            $image = BuildingImage::where(['building_id' => $building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 1])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 2])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        $building_ids = Building::where(['status' => 'active', 'type' => 'owner'])->pluck('id');
        if ($request->district_id && $request->building_type_id && $request->building_status_id) {
            $building_ids = Building::where(['status' => 'active', 'type' => 'owner','district_id'=>$request->district_id,'building_status_id'=>$request->building_status_id,'building_type_id'=>$request->building_type_id])->pluck('id');
        } elseif ($request->building_status_id && $request->building_type_id) {
            $building_ids = Building::where(['status' => 'active', 'type' => 'owner','building_status_id'=>$request->building_status_id,'building_type_id'=>$request->building_type_id])->pluck('id');
        } elseif ($request->building_type_id) {
            $building_ids = Building::where(['status' => 'active', 'type' => 'owner','building_type_id'=>$request->building_type_id])->pluck('id');
        }elseif ($request->building_status_id) {
            $building_ids = Building::where(['status' => 'active', 'type' => 'owner','building_status_id'=>$request->building_status_id])->pluck('id');
        }elseif ($request->district_id) {
            $building_ids = Building::where(['status' => 'active', 'type' => 'owner','district_id'=>$request->district_id])->pluck('id');
        }
        $flats = Flat::whereIn('building_id',$building_ids)->get();
        foreach ($flats as $flat) {
            $arr['id'] = (string)$flat->id;
            $arr['is_flat'] = '1';
            $favourite=Favourite::where(['user_id'=>$id,'flat_id'=>$flat->id])->first();
            if($favourite){
                $arr['favourite'] = true;
            }else{
                $arr['favourite'] = false;
            }
            $arr['building_name'] = $flat->name;
            $image = BuildingImage::where(['building_id' => $flat->building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$flat->building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function my_buildings($id,$lang)
    {
        $buildings = Building::where(['client_id'=>$id,'have_flats'=>'0','type'=>'owner'])->get();
        $data = [];
        $salary=0;
        foreach ($buildings as $building) {
            $arr['id'] = (string)$building->id;
            $favourite=Favourite::where(['user_id'=>$id,'building_id'=>$building->id])->first();
            if($favourite){
                $arr['favourite'] = true;
            }else{
                $arr['favourite'] = false;
            }
            $arr['is_flat'] = '0';
            $hiring=Hiring::where('building_id',$building->id)->first();
            if($hiring){
                $arr['hiring'] = true;
                $salary+=$building->rent;
            }else{
                $arr['hiring'] = false;
            }
            $image = BuildingImage::where(['building_id' => $building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 1])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 2])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        $building_ids = Building::where(['status' => 'active', 'type' => 'owner','client_id'=>$id,'have_flats'=>'1'])->pluck('id');

        $flats = Flat::whereIn('building_id',$building_ids)->get();
        foreach ($flats as $flat) {
            $arr['id'] = (string)$flat->id;
            $arr['is_flat'] = '1';
            $favourite=Favourite::where(['user_id'=>$id,'flat_id'=>$flat->id])->first();
            if($favourite){
                $arr['favourite'] = true;
            }else{
                $arr['favourite'] = false;
            }
            $hiring=Hiring::where('flat_id',$flat->id)->first();
            if($hiring){
                $arr['hiring'] = true;
                $salary+=$flat->rent;
            }else{
                $arr['hiring'] = false;
            }
            $arr['building_name'] = $flat->name;
            $image = BuildingImage::where(['building_id' => $flat->building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$flat->building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true,'salary'=>$salary, 'data' => $data]);
    }
    public function my_favourites($id,$lang)
    {
        $building_ids=Favourite::where('user_id',$id)->pluck('building_id');
        $flat_ids=Favourite::where('user_id',$id)->pluck('flat_id');

        $buildings = Building::whereIn('id',$building_ids)->get();
        $flats = Flat::whereIn('id',$flat_ids)->get();
        $data = [];
        foreach ($buildings as $building) {
            $arr['id'] = (string)$building->id;
            $arr['is_flat'] = '0';
            $image = BuildingImage::where(['building_id' => $building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 1])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 2])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        foreach ($flats as $flat) {
            $arr['id'] = (string)$flat->id;
            $arr['is_flat'] = '1';
            $arr['building_name'] = $flat->name;
            $image = BuildingImage::where(['building_id' => $flat->building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$flat->building->client_id)->value('name');
            if ($lang == 'ar') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }
        return response()->json(['value' => true, 'data' => $data]);
    }
    public function building($id,$lang)
    {
        if(!request('is_flat')){
            $is_flat='0';
        }else{
            $is_flat=request('is_flat');
        }
        if($is_flat== '1'){
            $flat = Flat::find($id);
            $arr['id'] = (string)$flat->id;
            $arr['favourite'] = false ;
            $image = BuildingImage::where(['building_id' => $flat->building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$flat->building->client_id)->value('name');
            $arr['building_name']=$flat->name;
            $arr['rent']=$flat->rent;
            if ($lang == 'ar') {
                $arr['city_name'] = CityDescription::where(['city_id' =>$flat->building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 1])->value('name');
                $arr['note'] = BuildingDescription::where(['building_id' => $flat->building->id, 'language_id' => 1])->value('note');
            } elseif ($lang == 'en') {
                $arr['city_name'] = CityDescription::where(['city_id' => $flat->building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $flat->building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $flat->building->building_status_id, 'language_id' => 2])->value('name');
                $arr['note'] = BuildingDescription::where(['building_id' => $flat->building->id, 'language_id' => 2])->value('note');
            }

            $images = BuildingImage::where(['building_id' => $flat->building->id])->get();
            $arr_i=[];
            foreach ($images as $image){
                $arr_i[] = asset('images/building/'.$image->name);
            }
            $arr['images']=$arr_i;
        }else{
            $building = Building::find($id);
            $arr['id'] = (string)$building->id;
            $image = BuildingImage::where(['building_id' => $building->id])->value('name');
            $arr['image'] = asset('images/building/'.$image);
            $arr['owner_name']=Client::where('id',$building->client_id)->value('name');
            $arr['rent']=$building->rent;
            if ($lang == 'ar') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 1])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 1])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 1])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 1])->value('name');
                $arr['note'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 1])->value('note');
            } elseif ($lang == 'en') {
                $arr['building_name'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 2])->value('name');
                $arr['city_name'] = CityDescription::where(['city_id' => $building->district->city_id, 'language_id' => 2])->value('name');
                $arr['district_name'] = DistrictDescription::where(['district_id' => $building->district_id, 'language_id' => 2])->value('name');
                $arr['status_name'] = BuildingStatusDescription::where(['building_status_id' => $building->building_status_id, 'language_id' => 2])->value('name');
                $arr['note'] = BuildingDescription::where(['building_id' => $building->id, 'language_id' => 2])->value('note');
            }

            $images = BuildingImage::where(['building_id' => $building->id])->get();
            $arr_i=[];
            foreach ($images as $image){
                $arr_i[] = asset('images/building/'.$image->name);
            }
            $arr['images']=$arr_i;
        }

        return response()->json(['value' => true, 'data' => $arr]);
    }
    public function months($id,$lang)
    {
        $user=User::find($id);
        $building=Building::find($user->building_id);
        $months=Month::where('building_id',$building->id)->orderBy('created_at','desc')->get();
        $data=[];
        foreach ($months as $month) {
            $arr['id'] = (string)$month->id;
            if ($lang == 'ar') {
                $arr['name'] = MonthDescription::where(['month_id' => $month->id, 'language_id' => 1])->value('name');
            } elseif ($lang == 'en') {
                $arr['name'] = MonthDescription::where(['month_id' => $month->id, 'language_id' => 2])->value('name');
            }
            $data[] = $arr;
        }

        return response()->json(['value' => true, 'data' => $data]);
    }
    public function union($id,$lang)
    {
        $user=User::find($id);
        $building = Building::find($user->building_id);
        if(!$building){
            return response()->json(['value' => false]);
        }
        $month=Month::where('building_id',$building->id)->orderBy('created_at','desc')->first();
        $users=User::where('building_id',$user->building_id)->get();
        $data=[];
        if ($lang == 'ar') {
            $month_name = MonthDescription::where(['month_id' => $month->id, 'language_id' => 1])->value('name');
            $note = MonthDescription::where(['month_id' => $month->id, 'language_id' => 1])->value('note');
        } elseif ($lang == 'en') {
            $month_name = MonthDescription::where(['month_id' => $month->id, 'language_id' => 2])->value('name');
            $note = MonthDescription::where(['month_id' => $month->id, 'language_id' => 2])->value('note');
        }
        $user_count=$users->count();
        $rent=$month->rent*$user_count;
        foreach ($users as $user){
            $arr['id'] = (string)$user->id;
            if($user->image){
                $arr['image'] = asset('images/user/'.$user->image);
            }else{
                $arr['image'] = '';
            }
            $arr['name']=$user->username;
            $arr['rent']=$month->rent;
            $baid=MonthBill::where(['month_id'=>$month->id,'user_id'=>$user->id,'confirm'=>'1'])->first();
            if($baid){
                $arr['baid'] = true;
            }else{
                $arr['baid'] = false;
            }
            $data[]=$arr;
        }

        return response()->json(['value' => true,'note'=>$note,'month_name'=>$month_name,'month_id'=>$month->id,'rent'=>$rent,'user_count'=>$user_count, 'data' => $data]);
    }
    public function about($lang)
    {
        if ($lang == 'ar') {
            $about = SettingDescription::where('language_id',1)->value('about');
        } elseif ($lang == 'en') {
            $about = SettingDescription::where('language_id',2)->value('about');
        }
        return response()->json(['value' => true,'data'=>$about]);
    }
}
