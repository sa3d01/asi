<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Article;
use App\Models\Language;
use App\Models\ArticleDescription;
use Illuminate\Http\Request;
use Analytics;
use Auth;
class ArticleController extends MasterController
{
    public function __construct(Article $model)
    {
        $this->model = $model;
        $this->route = 'article';
        $this->module_name         = 'قائمة الأخبار';
        $this->single_module_name  = 'خبر';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['title_'.$language->label] = 'required';
            $therulesarray['note_'.$language->label] = 'required';
        }
        return $therulesarray;
    }
    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $article=new Article();
        $article->add_by=request('add_by');
        $article->status=request('status');
        if(request('image')){
            $article->image=request('image');
        }
        $article->save();
        $languages=Language::all();
        foreach ($languages as $language){
            $article_description=new ArticleDescription();
            $article_description->title=request('title_'.$language->label);
            $article_description->note=request('note_'.$language->label);
            $article_description->language_id=$language->id;
            $article_description->article_id=$article->id;
            $article_description->save();
        }
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }

    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $article=$this->model->find($id);
//        $article->status=request('status');
        if(request('image')){
            $article->image=request('image');
        }
        $article->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $article_description=ArticleDescription::where(['article_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($article_description)){
                $article_description->title=request('title_'.$language->label);
                $article_description->note=request('note_'.$language->label);
                $article_description->update();
            }else{
                $article_description=new ArticleDescription();
                $article_description->title=request('title_'.$language->label);
                $article_description->note=request('note_'.$language->label);
                $article_description->language_id=$language->id;
                $article_description->article_id=$article->id;
                $article_description->save();
            }

        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }



}
