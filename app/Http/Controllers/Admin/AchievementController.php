<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Achievement;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Language;
use App\Models\AchievementDescription;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Carbon\Carbon;
use Analytics;
use Auth;
class AchievementController extends MasterController
{
    public function __construct(Achievement $model)
    {
        $this->model = $model;
        $this->route = 'achievement';
        $this->module_name         = 'قائمة الانجازات';
        $this->single_module_name  = 'انجاز';
        parent::__construct();
    }


    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['manager_'.$language->label] = 'required|max:255';
            $therulesarray['title_'.$language->label] = 'required|max:255';
            $therulesarray['note_'.$language->label] = 'required';
        }
        $therulesarray['date'] = 'required';
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $jachievement=new Achievement();
        $jachievement->manager=request('manager_ar');
        $jachievement->date=Carbon::parse(request('date'));
        $jachievement->save();
        $languages=Language::all();
        foreach ($languages as $language){
            $jachievement_description=new AchievementDescription();
            $jachievement_description->manager=request('manager_'.$language->label);
            $jachievement_description->title=request('title_'.$language->label);
            $jachievement_description->note=request('note_'.$language->label);
            $jachievement_description->language_id=$language->id;
            $jachievement_description->achievement_id=$jachievement->id;
            $jachievement_description->save();
        }
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $jachievement=$this->model->find($id);
        $jachievement->manager=request('manager_ar');
        $jachievement->date=Carbon::parse(request('date'));
        $jachievement->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $jachievement_description=AchievementDescription::where(['achievement_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($jachievement_description)){
                $jachievement_description->manager=request('manager_'.$language->label);
                $jachievement_description->title=request('title_'.$language->label);
                $jachievement_description->note=request('note_'.$language->label);
                $jachievement_description->update();
            }else{
                $jachievement_description=new AchievementDescription();
                $jachievement_description->manager=request('manager_'.$language->label);
                $jachievement_description->title=request('title_'.$language->label);
                $jachievement_description->note=request('note_'.$language->label);
                $jachievement_description->language_id=$language->id;
                $jachievement_description->achievement_id=$jachievement->id;
                $jachievement_description->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }



}
