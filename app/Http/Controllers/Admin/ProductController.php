<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Admin;
use App\Models\Product;
use App\Models\ProductDescription;
use App\Models\ProductImage;
use App\Models\Language;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Auth;
class ProductController extends MasterController
{
    public function __construct(Product $model)
    {
        $this->model = $model;
        $this->route = 'product';
        $this->module_name         = 'قائمة المنتجات';
        $this->single_module_name  = 'منتج';
        parent::__construct();
    }
    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['title_'.$language->label] = 'required';
            $therulesarray['note_'.$language->label] = 'required';
        }
        return $therulesarray;
    }
    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $product=new Product();
        $product->add_by=request('add_by');
        $product->status=request('status');
        $product->type=request('type');
        if($request->video){
            $product->video=$request->video;
        }
        if(request('android')){
            $product->android=request('android');
        }
        if(request('ios')){
            $product->ios=request('ios');
        }
        $product->save();
        $languages=Language::all();
        foreach ($languages as $language){
            $product_description=new ProductDescription();
            $product_description->title=request('title_'.$language->label);
            $product_description->note=request('note_'.$language->label);
            $product_description->language_id=$language->id;
            $product_description->product_id=$product->id;
            $product_description->save();
        }
        if($request->images){
            foreach ($request->images as $image){
                $product_image=new ProductImage();
                $product_image->add_by=request('add_by');
                $product_image->product_id=$product->id;
                $product_image->name=$image;
                $product_image->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }

    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $product=$this->model->find($id);
        if(request('android')){
            $product->android=request('android');
        }
        if(request('ios')){
            $product->ios=request('ios');
        }
        $product->type=request('type');
        if($request->video){
            $product->video=$request->video;
        }
        $product->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $product_description=ProductDescription::where(['product_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($product_description)){
                $product_description->title=request('title_'.$language->label);
                $product_description->note=request('note_'.$language->label);
                $product_description->update();
            }else{
                $product_description=new ProductDescription();
                $product_description->title=request('title_'.$language->label);
                $product_description->note=request('note_'.$language->label);
                $product_description->language_id=$language->id;
                $product_description->product_id=$product->id;
                $product_description->save();
            }

        }
        if($request->images){
            $images=ProductImage::where('product_id',$product->id)->get();
            foreach ($images as $image){
                $image->delete();
            }
            foreach ($request->images as $image){
                $product_image=new ProductImage();
                $product_image->add_by=request('add_by');
                $product_image->product_id=$product->id;
                $product_image->name=$image;
                $product_image->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }
}
