<?php

namespace App\Http\Controllers\Admin;
use App\Models\Bank;
use App\Models\BankDescription;
use App\Models\Language;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Auth;
class BankController extends MasterController
{
    public function __construct(Bank $model)
    {
        $this->model = $model;
        $this->route = 'bank';
        $this->module_name         = 'قائمة الحسابات البنكية';
        $this->single_module_name  = 'حساب بنكى';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['name_'.$language->label] = 'required|max:255';
        }
        $therulesarray['logo'] ='mimes:png,jpg,jpeg';
        $therulesarray['number'] = 'required';
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $bank=new Bank();
        $bank->add_by=request('add_by');
        $bank->status=request('status');
        $bank->number=request('number');
        $bank->logo=request('logo');
        $bank->save();
        $languages=Language::all();
        foreach ($languages as $language){
            $bank_description=new BankDescription();
            $bank_description->name=request('name_'.$language->label);
            $bank_description->language_id=$language->id;
            $bank_description->bank_id=$bank->id;
            $bank_description->save();
        }

        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $bank=$this->model->find($id);
//        $bank->status=request('status');
        $bank->number=request('number');
        $bank->logo=request('logo');
        $bank->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $bank_description=BankDescription::where(['bank_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($bank_description)){
                $bank_description->name=request('name_'.$language->label);
                $bank_description->update();
            }else{
                $bank_description=new BankDescription();
                $bank_description->name=request('name_'.$language->label);
                $bank_description->language_id=$language->id;
                $bank_description->bank_id=$bank->id;
                $bank_description->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }
}