<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Language;
use App\Models\FeedbackDescription;
use App\Models\Feedback;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Analytics;
use Auth;
class FeedbackController extends MasterController
{
    public function __construct(Feedback $model)
    {
        $this->model = $model;
        $this->route = 'feedback';
        $this->module_name         = 'قائمة اراء العمﻻء';
        $this->single_module_name  = 'رأى';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['title_'.$language->label] = 'required';
            $therulesarray['note_'.$language->label] = 'required';
        }
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $building=new Feedback();
        $building->add_by=request('add_by');
        $building->status=request('status');
        if(request('image')){
            $building->image=request('image');
        }
        $building->save();

        $languages=Language::all();
        foreach ($languages as $language){
            $building_description=new FeedbackDescription();
            $building_description->title=request('title_'.$language->label);
            $building_description->note=request('note_'.$language->label);
            $building_description->language_id=$language->id;
            $building_description->feedback_id=$building->id;
            $building_description->save();
        }

        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $building=$this->model->find($id);
//        $building->status=request('status');
        if(request('image')){
            $building->image=request('image');
        }
        $building->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $building_description=FeedbackDescription::where(['feedback_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($building_description)){
                $building_description->title=request('title_'.$language->label);
                $building_description->note=request('note_'.$language->label);                $building_description->update();
            }else{
                $building_description=new FeedbackDescription();
                $building_description->title=request('title_'.$language->label);
                $building_description->note=request('note_'.$language->label);                $building_description->language_id=$language->id;
                $building_description->feedback_id=$building->id;
                $building_description->save();
            }
        }

        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }

}
