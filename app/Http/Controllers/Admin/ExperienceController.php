<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Experience;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Language;
use App\Models\ExperienceDescription;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Analytics;
use Auth;
class ExperienceController extends MasterController
{
    public function __construct(Experience $model)
    {
        $this->model = $model;
        $this->route = 'experience';
        $this->module_name         = 'قائمة الخبرات';
        $this->single_module_name  = 'خبرة';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
//            $therulesarray['title_'.$language->label] = 'required|max:255';
            $therulesarray['note_'.$language->label] = 'required|max:255';
        }
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $Experience=new Experience();
        $Experience->add_by=request('add_by');
        $Experience->save();

        $languages=Language::all();
        foreach ($languages as $language){
            $ExperienceDescription=new ExperienceDescription();
            $ExperienceDescription->title=request('title_'.$language->label);
            $ExperienceDescription->note=request('note_'.$language->label);
            $ExperienceDescription->language_id=$language->id;
            $ExperienceDescription->Experience_id=$Experience->id;
            $ExperienceDescription->save();
        }

        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $Experience=$this->model->find($id);
        $languages=Language::all();
        foreach ($languages as $language){
            $ExperienceDescription=ExperienceDescription::where(['experience_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($ExperienceDescription)){
//                $ExperienceDescription->title=request('title_'.$language->label);
                $ExperienceDescription->note=request('note_'.$language->label);
                $ExperienceDescription->update();
            }else{
                $ExperienceDescription=new ExperienceDescription();
//                $ExperienceDescription->title=request('title_'.$language->label);
                $ExperienceDescription->note=request('note_'.$language->label);                $ExperienceDescription->language_id=$language->id;
                $ExperienceDescription->Experience_status_id=$Experience->id;
                $ExperienceDescription->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }



}
