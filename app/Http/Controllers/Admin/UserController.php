<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Ad;
use App\Models\Admin;
use App\Models\Category;
use App\Models\City;
use App\Models\Client;
use App\Models\Country;
use App\Models\Product;
use App\Models\SubCategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Analytics;
use Auth;
class UserController extends MasterController
{
    public function __construct(User $model)
    {
        $this->model = $model;
        $this->route = 'user';
        $this->module_name         = 'قائمة الأعضاء';
        $this->single_module_name  = 'عضو';
        $this->index_fields        = ['الاسم' => 'username','رقم الجوال' => 'mobile','البريد الالكترونى' => 'email'];
        $this->create_fields        = ['الاسم' => 'username','رقم الجوال' => 'mobile','البريد الالكترونى' => 'email'];
        $this->update_fields        = ['الاسم' => 'username','رقم الجوال' => 'mobile','البريد الالكترونى' => 'email'];
        parent::__construct();
    }


    public function validation_func($method,$id=null)
    {
        if($method == 1) // POST Case
            return ['name' => 'required', 'mobile' => 'required|unique:users','email'=>'email|max:255|unique:users', 'image' => 'mimes:png,jpg,jpeg','password'=>'required|min:6'];
        return ['name' => 'required', 'mobile' => 'required|unique:users,mobile,'.$id,'email'=>'email|max:255|unique:users,email,'.$id, 'image' => 'mimes:png,jpg,jpeg'];
    }



}
