<?php

namespace App\Http\Controllers\Admin;
use App\Models\Host;
use App\Models\HostDescription;
use App\Models\Language;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Auth;
class HostController extends MasterController
{
    public function __construct(Host $model)
    {
        $this->model = $model;
        $this->route = 'host';
        $this->module_name         = 'قائمة الاستضافات';
        $this->single_module_name  = 'استضافة';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
//        foreach ( $languages as $language) {
//            $therulesarray['title_'.$language->label] = 'required|max:255';
//            $therulesarray['note_'.$language->label] = 'required';
//        }
        $therulesarray['price'] = 'required';
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $host=new Host();
        $host->add_by=request('add_by');
        $host->status=request('status');
        $host->price=request('price');
        $host->save();
        $languages=Language::all();
        foreach ($languages as $language){
            $host_description=new HostDescription();
            $host_description->title=request('title_'.$language->label);
            $host_description->note=request('note_'.$language->label);
            $host_description->language_id=$language->id;
            $host_description->host_id=$host->id;
            $host_description->save();
        }
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $host=$this->model->find($id);
//        $host->status=request('status');
        $host->price=request('price');
        $host->update();
//        $languages=Language::all();
//        foreach ($languages as $language){
//            $host_description=HostDescription::where(['host_id'=>$id,'language_id'=>$language->id])->first();
//            if(isset($host_description)){
//                $host_description->title=request('title_'.$language->label);
//                $host_description->note=request('note_'.$language->label);
//                $host_description->update();
//            }else{
//                $host_description=new HostDescription();
//                $host_description->title=request('title_'.$language->label);
//                $host_description->note=request('note_'.$language->label);                $host_description->language_id=$language->id;
//                $host_description->host_id=$host->id;
//                $host_description->save();
//            }
//        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }
}