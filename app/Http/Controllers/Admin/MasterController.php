<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Achievement;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Team;
use App\Models\Partener;
use App\Models\Jop;
use App\Models\Language;
use App\Models\Feedback;
use App\Models\Experience;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Social;
use App\Models\Article;
use App\Models\Branche;
use App\User;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use Auth;
//use Analytics;
//use Spatie\Analytics\Period;

abstract class MasterController extends Controller {

    protected $model;         // model name
    protected $other_model;         // other_model name
    protected $route;         // route name
    protected $perPage = 10;  // pagination
    protected $validation_c;  // validation on create
    protected $validation_u;  // validation on update
    protected $module_name;
    protected $single_module_name;
    protected $index_fields;
    protected $create_fields;
    protected $update_fields;

    public function __construct()
    {
        $clients_array = [];
        $clients=Client::where('status','active')->get();
        foreach ( $clients as $client) {
            $clients_array[$client->id] = $client->name;
        }
        $this->middleware('auth:admin');
        view()->share(array(
            'module_name'        => $this->module_name,
            'single_module_name' => $this->single_module_name,
            'route'              => $this->route,
            'index_fields'       => $this->index_fields,
            'create_fields'      => $this->create_fields,
            'update_fields'      => $this->update_fields,
            'status'        => ['active'=>'مفعل','not_active'=>'غير مفعل'],
            'two_user_types'        => ['union'=>'اتحاد مﻻك','user'=>'مستخدم عادى'],
            'two_building_types'        => ['union'=>'اتحاد مﻻك','owner'=>'للايجار'],
            'types'        => ['mobile'=>'تطبيقات موبايل','web'=>'تطوير المواقع','networking'=>'الشبكات','marketing'=>'التسويق'],
            'clients'        => $clients_array,
            'languages'        => Language::all(),
            'setting'        => Setting::first(),
            'admin_count'=>Admin::count(),
            'user_count'=>User::count(),
            'client_count'=>Client::count(),
            'page_count'=>Page::count(),
            'lang_count'=>Language::count(),
            'social_count'=>Social::count(),
            'achievement_count'=>Achievement::count(),
            'article_count'=>Article::count(),
            'experience_count'=>Experience::count(),
            'feedback_count'=>Feedback::count(),
            'jop_count'=>Jop::count(),
            'partener_count'=>Partener::count(),
            'product_count'=>Product::count(),
            'team_count'=>Team::count(),
            'branche_count'=>Branche::count(),
            'con_count'=>Contact::count(),
        ));

    }

	public function index() {
        $rows = $this->model->latest()->get();
        return view('admin.'.$this->route.'.index', compact('rows'));
    }

    public function create() {
        return view('admin.'.$this->route.'.create');   
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func(1));
        $this->model->create($request->all());
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }

    public function edit($id) {
        $row = $this->model->find($id);
        return View('admin.'.$this->route.'.edit', compact('row'));
    }

    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func(2,$id));
        $this->model->find($id)->update($request->all());
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }

    public function destroy($id) {
        $this->model->find($id)->delete();
        return redirect('admin/'.$this->route.'')->with('deleted','تم الحذف بنجاح');
    }

    public function show($id)
    {
        $row = $this->model->find($id);
        return View('admin.'.$this->route.'.show', compact('row'));
    }

    public function Activate($id) {
        $row = $this->model->find($id);
        if($row->status == 'not_active') {
            $this->model->find($id)->update(['status' => 'active']);
            return back()->with('success' ,'تم الغاء التفعيل بنجاح');
        }
        else{
            $this->model->find($id)->update(['status' => 'not_active']);
            return back()->with('success' , 'تم التفعيل بنجاح');
        }
    }

}

