<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Language;
use App\Models\TeamDescription;
use App\Models\Team;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Auth;
class TeamController extends MasterController
{
    public function __construct(Team $model)
    {
        $this->model = $model;
        $this->route = 'team';
        $this->module_name         = 'قائمة الأعضاء';
        $this->single_module_name  = 'عضو';
        parent::__construct();
    }

    public function validation_func()
    {
        $languages=Language::all();
        $therulesarray = [];
        foreach ( $languages as $language) {
            $therulesarray['job_'.$language->label] = 'required|max:255';
            $therulesarray['name_'.$language->label] = 'required|max:255';
        }
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $team=new Team();
        $team->name=request('name_ar');
        if(request('image')){
            $team->image=request('image');
        }
        $team->save();

        $languages=Language::all();
        foreach ($languages as $language){
            $team_description=new TeamDescription();
            $team_description->name=request('name_'.$language->label);
            $team_description->job=request('job_'.$language->label);
            $team_description->language_id=$language->id;
            $team_description->team_id=$team->id;
            $team_description->save();
        }

        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $this->validate($request, $this->validation_func());
        $team=$this->model->find($id);
        $team->name=request('name_ar');
        if(request('image')){
            $team->image=request('image');
        }        $team->update();
        $languages=Language::all();
        foreach ($languages as $language){
            $team_description=TeamDescription::where(['team_id'=>$id,'language_id'=>$language->id])->first();
            if(isset($team_description)){
                $team_description->name=request('name_'.$language->label);
                $team_description->job=request('job_'.$language->label);
                $team_description->update();
            }else{
                $team_description=new TeamDescription();
                $team_description->name=request('name_'.$language->label);
                $team_description->job=request('job_'.$language->label);
                $team_description->language_id=$language->id;
                $team_description->team_id=$team->id;
                $team_description->save();
            }
        }
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }



}
