<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MasterController;
use App\Models\Partener;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Language;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Analytics;
use Auth;
class PartenerController extends MasterController
{
    public function __construct(Partener $model)
    {
        $this->model = $model;
        $this->route = 'partener';
        $this->module_name         = 'قائمة الشركاء';
        $this->single_module_name  = 'شريك';
        parent::__construct();
    }
    public function validation_func()
    {
        $therulesarray = [];
        $therulesarray['image'] = 'required';
        return $therulesarray;
    }

    public function store(Request $request) {
        $this->validate($request, $this->validation_func());
        $partener=new Partener();
        $partener->add_by=request('add_by');
        $partener->image=request('image');
        $partener->status=request('status');
        $partener->save();
        return redirect('admin/'.$this->route.'')->with('created', 'تمت الاضافة بنجاح');
    }


    public function update($id, Request $request) {
        $partener=$this->model->find($id);
        if(request('image')){
            $partener->image=request('image');
        }
        $partener->update();
        return redirect('admin/'.$this->route.'')->with('updated','تم التعديل بنجاح');
    }



}
