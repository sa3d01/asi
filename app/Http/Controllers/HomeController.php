<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Setting;
use App\Models\Team;
use App\Models\Achievement;
use App\Models\Product;
use App\Models\Partener;
use App\Models\Experience;
use App\Models\Article;
use App\Models\Host;
use App\Models\Jop;
use App\Models\Branche;
use App\Models\ArticleDescription;
use Illuminate\Http\Request;
use Goutte;
use Illuminate\Support\Facades\Artisan;
use Cocur\Domain\Connection\ConnectionFactory;
use Cocur\Domain\Data\DataLoader;
use Cocur\Domain\Whois\Client as WhoisClient;
use Cocur\Domain\Availability\Client as AvailabilityClient;
use Cocur\Domain\Whois\Client;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $setting=Setting::first();
//        $this->middleware('auth:admin');
    if(\App::isLocale('ar'))
{
            $language_id=1;
        }else{
            $language_id=2;
        }

        view()->share(array(
            'setting'=>$setting,
            'language_id'=>$language_id
        ));

    }
    public static function lang(){
        if (Session::has('language')) {
            if (Session::get('language')  == 'ar') {
                $lang='ar';
            }else{
                $lang='en';
            }
        }else{
            $lang='ar';
        }
        App::setLocale($lang);
        return $lang;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams =Team::all();
        $acs=Achievement::all();
        $exps=Experience::all();
        $products=Product::all(); 
        $parts=Partener::all();
       // dd($exps);
        return view('web.index',compact('teams','acs','products','parts'));
    }
    public function search(Request $request)
    {
        $search=$request->search;
        $new_ids=ArticleDescription::where('title','like','%'.$search)->orWhere('title','like',$search.'%')->pluck('article_id');
        $news=Article::whereIn('id',$new_ids)->get();
        return view('web.news',compact('news'));
    }
    public function domain(Request $request)
    {
        $domain = $request->domain;
        $factory = new ConnectionFactory();
        $dataLoader = new DataLoader();
        $data = $dataLoader->load(__DIR__.'/tld.json');
        $client = new Client($factory, $data);
        $string= $client->query($domain);
        substr($string, 0, 11);
        if(substr($string, 0, 12) == "No match for"){
            $available=true;
        }else{
            $available=false;
        }
        $hosts=Host::all();
        return view('web.hosting',compact('hosts','available','domain'));
    }


    public function singleProject($id){
        $product=Product::find($id);
        return view('web.singleProject',compact('product'));
    }
    public function about()
    {
        return view('web.about');
    }
    public function singleArtice($id){
        $new = Article::find($id);
        return view('web.singleNew',compact('new'));
    }

    public function contact()
    {
        $branches=Branche::where('status','active')->get();
        return view('web.contact-us',compact('branches'));
    }

    public function getProjects()
    {
        $web_products=Product::where(['status'=>'active','type'=>'web'])->get();
        $mobile_products=Product::where(['status'=>'active','type'=>'mobile'])->get();
        $networking_products=Product::where(['status'=>'active','type'=>'networking'])->get();
        $marketing_products=Product::where(['status'=>'active','type'=>'marketing'])->get();
        return view('web.projects',compact('web_products','marketing_products','mobile_products','networking_products'));
    }

    public function getHosting()
    {
        $hosts=Host::all();
        return view('web.hosting',compact('hosts'));
    }

    public function getNews()
    {
        $news=Article::all();
        return view('web.news',compact('news'));
    }

    public function getJobs()
    {
        $jops=Jop::where('status','active')->get();
        return view('web.jobs',compact('jops'));
    }
    public function postContact(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'message'=>'required'
        ]);
        $item=new Contact();
        $item->name=$request['name'];
        $item->email=$request['email'];
        $item->phone=$request['phone'];
        $item->message=$request['message'];
        $item->save();
        return back();
    }
}
