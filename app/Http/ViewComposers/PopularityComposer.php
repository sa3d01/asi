<?php
/**
 * Created by PhpStorm.
 * User: sa3d01
 * Date: 21/02/18
 * Time: 01:01 م
 */

namespace App\Http\ViewComposers;

use App\Services\Trending;
use Illuminate\View\View;

class PopularityComposer
{
    private $trending;

    public function __construct(Trending $trending)
    {
        $this->trending = $trending;
    }

    public function compose(View $view)
    {
        $view->with('popular', $this->trending->week());
    }
}