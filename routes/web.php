<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use Analytics;
//use Spatie\Analytics\Period;
/*app()->singleton('lang',function(){
    return App\Http\Controllers\HomeController::lang();
});
app()->singleton('language_id',function($lang){
    if($lang=='ar'){
        $language_id=1;
    }else{
        $language_id=2;
    }
    return $language_id;
});
Route::get('lang/{lang}',function($lang){
    if($lang == 'ar'){
        session()->put('lang','ar');
    }elseif($lang == 'en'){
        session()->put('lang','en');
    }else{
        session()->put('lang','ar');
    }
    App::setLocale($lang);
    return back();
});
*/
Route::get('/', 'HomeController@index')->name('index');
Route::post('/search', 'HomeController@search')->name('search');
Route::post('/domain', 'HomeController@domain')->name('domain');
Route::get('contactUs', 'HomeController@contact')->name('Contact_Us');
Route::get('projects', 'HomeController@getProjects')->name('Projects');
Route::get('project/{id}', 'HomeController@singleProject')->name('Project');
Route::get('hosting', 'HomeController@getHosting')->name('HOST');
Route::get('/news','HomeController@getNews')->name('NEWS');
Route::get('/new/{id}','HomeController@singleArtice')->name('Single.New');
Route::get('/jobs','HomeController@getJobs')->name('Jobs');
Route::post('/sendContact','HomeController@postContact')->name('POST_SEND_CONTACT');
Route::get('/changeLanguage/{lang}','LanguageController@getChangeLanguage')->name('CHANGE_LANGUAGE')->middleware('lang');
//Route::get('/',function (){
//    return Analytics::fetchVisitorsAndPageViews(Period::days(7));
//
//});
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();

//admin password reset routes
Route::group(['prefix'=>'/admin','namespace'=>'Auth'],function() {
    Route::get('/password/reset','AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email','AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('/password/reset','AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}','AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});
Route::group(['prefix'=>'/admin','namespace'=>'Admin'],function() {
    //auth
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'AdminLoginController@logout')->name('admin.logout');

    //dashboard
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');

    Route::get('/profile', 'AdminController@edit_profile')->name('edit_profile');

    Route::get('setting', 'SettingController@get_setting')->name('setting.get_setting');
    Route::patch('setting/{id}', 'SettingController@update_setting')->name('setting.update_setting');

    Route::resource('admin', 'AdminController');
    Route::post('admin/activate/{id}', 'AdminController@activate')->name('active_admin');

    Route::resource('achievement', 'AchievementController');
    Route::post('achievement/activate/{id}', 'AchievementController@activate')->name('active_achievement');

    Route::resource('article', 'ArticleController');
    Route::post('article/activate/{id}', 'ArticleController@activate')->name('active_article');

    Route::resource('user', 'UserController');
    Route::post('user/activate/{id}', 'UserController@activate')->name('active_user');

    Route::resource('experience', 'ExperienceController');
    Route::post('experience/activate/{id}', 'ExperienceController@activate')->name('active_experience');

    Route::resource('social', 'SocialController');
    Route::post('social/activate/{id}', 'SocialController@activate')->name('active_social');

    Route::resource('feedback', 'FeedbackController');
    Route::post('feedback/activate/{id}', 'FeedbackController@activate')->name('active_feedback');

    Route::resource('host', 'HostController');
    Route::post('host/activate/{id}', 'HostController@activate')->name('active_host');

    Route::resource('jop', 'JopController');
    Route::post('jop/activate/{id}', 'JopController@activate')->name('active_jop');

    Route::resource('partener', 'PartenerController');
    Route::post('partener/activate/{id}', 'PartenerController@activate')->name('active_partener');

    Route::resource('product', 'ProductController');
    Route::post('product/activate/{id}', 'ProductController@activate')->name('active_product');

    Route::resource('team', 'TeamController');
    Route::post('team/activate/{id}', 'TeamController@activate')->name('active_team');

    Route::resource('page', 'PageController');
    Route::post('page/activate/{id}', 'PageController@activate')->name('active_page');

    Route::resource('branche', 'BrancheController');
    Route::post('branche/activate/{id}', 'BrancheController@activate')->name('active_branche');

    Route::resource('language', 'LanguageController');
    Route::resource('contact', 'ContactController');
    Route::post('language/activate/{id}', 'LanguageController@activate')->name('active_language');
});

//User View Routes
