<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1'], function () {

    Route::post('/signup/{lang}', 'Api\UserController@signup');
    Route::put('/update_profile/{id}/{lang}', 'Api\UserController@update_profile');

    Route::post('/signin/{lang}', 'Api\UserController@signin');
    Route::post('/confirmation_sms/{lang}', 'Api\UserController@confirmation_sms');

    Route::any('/buildings/{id?}/{lang}', 'Api\BuildingController@buildings');
    Route::get('/my_buildings/{id}/{lang}', 'Api\BuildingController@my_buildings');
    Route::any('/building/{id}/{lang}', 'Api\BuildingController@building');
    Route::any('/my_favourites/{id}/{lang}', 'Api\BuildingController@my_favourites');
    Route::any('/months/{id}/{lang}', 'Api\BuildingController@months');
    Route::get('/union/{id}/{lang}', 'Api\BuildingController@union');
    Route::get('/about/{lang}', 'Api\BuildingController@about');
    Route::get('/banks/{lang}', 'Api\ContactController@banks');
    Route::post('/contact', 'Api\ContactController@contact');
    Route::post('/favourite', 'Api\ContactController@favourite');
    Route::post('/bill', 'Api\ContactController@bill');
    Route::get('/building_status/{lang}', 'Api\ContactController@building_status');
    Route::get('/building_type/{lang}', 'Api\ContactController@building_type');
    Route::get('/city/{lang}', 'Api\ContactController@city');
    Route::get('/district/{id}/{lang}', 'Api\ContactController@district');




});