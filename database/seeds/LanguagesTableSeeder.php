<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'name' => 'اللغة العربية',
            'label' => 'ar',
            'status' => 'active',
        ]);
        DB::table('languages')->insert([
            'name' => 'English',
            'label' => 'en',
            'status' => 'active',
        ]);
    }
}
