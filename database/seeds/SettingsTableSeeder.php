<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'logo' => 'logo.png',
            'email' => 'admin@admin.com',
            'mobile_1' => '01092291228',
            'mobile_2' => '01122435541',
        ]);
        DB::table('setting_descriptions')->insert([
            'setting_id' => '1',
            'language_id' => '1',
            'name' => 'asi',
        ]);
        DB::table('setting_descriptions')->insert([
            'setting_id' => '1',
            'language_id' => '2',
            'name' => 'asi',
        ]);

    }
}
