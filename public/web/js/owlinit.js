


// OWL Carousel 

    const bq = window.matchMedia( "(min-width: 768px)" );
    if (bq.matches) {
$('.works-slider').owlCarousel({


     rtl:true,
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    navText: ["<i class='ion-ios-arrow-right'></i>","<i class='ion-ios-arrow-left'></i>"],
    responsive:{
        0:{
            items:1
        }
    }
    
});
}

    const mq = window.matchMedia( "(max-width: 767px)" );
    if (mq.matches) {
$('.works-slider').owlCarousel({
        animateOut: 'fadeOutDown',
    animateIn: 'fadeInUp',
//    autoplay:true,
     rtl:true,
    loop:true,
    margin:10,
//    autoplayHoverPause: true,
    nav:true,
    dots:false,
    navText: ["<i class='ion-ios-arrow-right'></i>","<i class='ion-ios-arrow-left'></i>"],
    responsive:{
        0:{
            items:1
        }
    }
    
});
}


// testimonials Carousel

$("#testimonial-slider").owlCarousel({
    rtl:true,
    loop:true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    margin:10,
    nav:false,
    dots:true,
    autoplay:true,
        responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
    });

// partners carousel

$('.partners-slider').owlCarousel({
    rtl:true,
    loop:true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    margin:10,
    nav:false,
    dots:false,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        350:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
